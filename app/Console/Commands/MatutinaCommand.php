<?php

namespace App\Console\Commands;

use App\Http\Controllers\DrawController;
use Illuminate\Console\Command;

class MatutinaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draws:matutina';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Matutina';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $drawController = new DrawController();
        $drawController->getMatutina();
    }
}
