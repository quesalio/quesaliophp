<?php

namespace App\Console\Commands;

use App\Http\Controllers\ConfigurationController;
use Illuminate\Console\Command;

class StartVespertinaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draw:start_vespertina';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start draw for vespertina';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $configuration=new ConfigurationController();
        $configuration->startDraw('vespertina');
    }
}
