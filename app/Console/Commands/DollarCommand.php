<?php

namespace App\Console\Commands;

use App\Http\Controllers\DollarController;
use Illuminate\Console\Command;

class DollarCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dollar:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get dollar values';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dollar = new DollarController();
        $dollar->getDollarExchange();
    }
}
