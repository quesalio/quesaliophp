<?php

namespace App\Console\Commands;

use App\Http\Controllers\ConfigurationController;
use Illuminate\Console\Command;

class StopDrawCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draw:stop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stop draw for all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $configuration = new ConfigurationController();
        $configuration->stopDraw();
    }
}
