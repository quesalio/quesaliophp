<?php

namespace App\Console\Commands;

use App\Http\Controllers\DrawController;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class NocturnaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draws:nocturna';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Nocturna';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $drawController = new DrawController();
        $drawController->getNocturna();
    }
}
