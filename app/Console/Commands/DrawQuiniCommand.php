<?php

namespace App\Console\Commands;

use App\Http\Controllers\DrawController;
use Illuminate\Console\Command;

class DrawQuiniCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draw:quini';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch quini 6 data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $drawController = new DrawController();
        $drawController->getQuini6();
    }
}
