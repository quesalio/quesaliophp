<?php

namespace App\Console;

use App\Console\Commands\CurrentDateCommand;
use App\Console\Commands\DollarCommand;
use App\Console\Commands\DrawBrincoCommand;
use App\Console\Commands\DrawLotoCommand;
use App\Console\Commands\DrawPoceadaCommand;
use App\Console\Commands\DrawPoceadaPlusCommand;
use App\Console\Commands\DrawQuiniCommand;
use App\Console\Commands\MatutinaCommand;
use App\Console\Commands\NocturnaCommand;
use App\Console\Commands\NotificationMatutinaCommand;
use App\Console\Commands\NotificationNocturnaCommand;
use App\Console\Commands\NotificationPrimeraCommand;
use App\Console\Commands\NotificationVespertinaCommand;
use App\Console\Commands\PrimeraCommand;
use App\Console\Commands\SchemaCommand;
use App\Console\Commands\StartMatutinaCommand;
use App\Console\Commands\StartNocturnaCommand;
use App\Console\Commands\StartPrimeraCommand;
use App\Console\Commands\StartVespertinaCommand;
use App\Console\Commands\StopDrawCommand;
use App\Console\Commands\VespertinaCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        PrimeraCommand::class,
        MatutinaCommand::class,
        VespertinaCommand::class,
        NocturnaCommand::class,
        NotificationPrimeraCommand::class,
        NotificationMatutinaCommand::class,
        NotificationVespertinaCommand::class,
        NotificationNocturnaCommand::class,
        DrawPoceadaCommand::class,
        DrawPoceadaPlusCommand::class,
        DrawBrincoCommand::class,
        DrawLotoCommand::class,
        DrawQuiniCommand::class,
        StartPrimeraCommand::class,
        StartMatutinaCommand::class,
        StartVespertinaCommand::class,
        StartNocturnaCommand::class,
        StopDrawCommand::class,
        DollarCommand::class,
        CurrentDateCommand::class,
        SchemaCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('draws:primera')->cron('05,10,15,20,25,31,33,35,37,39,41,43,45,47,49,51,55,59 11,12 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draws:matutina')->cron('01,02,04,06,08,10,12,14,16,18,20,23,26,29,30,35,40,45,50,59 14,15 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draws:vespertina')->cron('31,33,35,37,39,41,43,45,47,49,50,55,59 17,18 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draws:nocturna')->cron('01,02,04,06,08,10,12,14,16,18,20,23,26,29,30,35,40,45,50,59 21,22,23 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        //notifications
        $schedule->command('notification:primera')->cron('06,11,16,21,26,36,38,40,42,44,46,48,50,52,56,59 11,12 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('notification:matutina')->cron('05,07,09,11,13,15,17,19,21,24,27,30,31,36,41,46,51,59 14,15 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('notification:vespertina')->cron('36,38,40,42,44,46,48,50,51,56,59 17,18 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('notification:nocturna')->cron('05,07,09,11,13,15,17,19,21,24,27,30,31,36,41,46,51,59 21,22 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        //draws
        $schedule->command('draw:poceada')->cron('15,18,20,23,25,30,35,40,59 21 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:poceada_plus')->cron('15,18,20,23,25,30,35,45,59 21 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:loto')->cron('15,20,35,58 22 * * 0,3')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:quini')->cron('25,30,35,40,45,58 21 * * 0,3')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:brinco')->cron('15,20,35,58 21 * * 0')->timezone('America/Argentina/Buenos_Aires');
        //start and stop draws
        $schedule->command('draw:start_primera')->cron('30 11 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:stop')->cron('45 11 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:start_matutina')->cron('00 14 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:stop')->cron('15 14 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:start_vespertina')->cron('30 15 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:stop')->cron('45 15 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:start_nocturna')->cron('00 21 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        $schedule->command('draw:stop')->cron('15 21 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        //dollar value
        $schedule->command('dollar:get')->cron('3,6,12,18,21 0 * * *')->timezone('America/Argentina/Buenos_Aires');
        //current date
        $schedule->command('current:date')->cron('30 11 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        //schema
        $schedule->command('schema:get')->cron('05 00 * * 1-6')->timezone('America/Argentina/Buenos_Aires');
        */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
