<?php

namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Poceada extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "poceada";
    protected $dates = ['date'];
}
