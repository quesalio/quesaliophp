<?php

use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class GeneralHelper
{
    function sendIdsToSubscription($ids,$type)
    {
        $url = config('general.graphql_subscription_url_for_query');
        switch ($type){
            case 'primera':
                $query = 'query{primera_modified (ids:"' . $ids . '"){city,value,id_loteria,id_draw,date,meaning,meaning_image,meaning_number,numbers{number,value}}}';
                break;
            case 'matutina':
                $query = 'query{matutina_modified (ids:"' . $ids . '"){city,value,id_loteria,id_draw,date,meaning,meaning_image,meaning_number,numbers{number,value}}}';
                break;
            case 'vespertina':
                $query = 'query{vespertina_modified (ids:"' . $ids . '"){city,value,id_loteria,id_draw,date,meaning,meaning_image,meaning_number,numbers{number,value}}}';
                break;
            case 'nocturna':
                $query = 'query{nocturna_modified (ids:"' . $ids . '"){city,value,id_loteria,id_draw,date,meaning,meaning_image,meaning_number,numbers{number,value}}}';
                break;
        }
        $new_url = $url . urlencode($query);
        $goutteClient = new Client();
        /*$guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);*/
        $goutteClient->request('GET', $new_url);
    }

    public static function instance()
    {
        return new GeneralHelper();
    }
}