<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Dollar extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "dollar";
}
