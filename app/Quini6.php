<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Quini6 extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "quini6";
    protected $dates = ['date'];
}
