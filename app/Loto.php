<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Loto extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "loto";
    protected $dates = ['date'];
}

