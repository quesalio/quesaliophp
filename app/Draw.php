<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Draw extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "draws";
    protected $dates = ['date'];
}
