<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Nocturna extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "nocturna";
    protected $dates = ["date"];
    public function numbers()
    {
        return $this->embedsMany(NocturnaDetail::class);
    }
}
