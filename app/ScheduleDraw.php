<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ScheduleDraw extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "schedule_draw";
    protected $dates = ['date'];
}
