<?php

namespace App\Http\Controllers;

use App\Brinco;
use App\Draw;
use App\Loto;
use App\Matutina;
use App\MatutinaDetail;
use App\Meaning;
use App\Nocturna;
use App\NocturnaDetail;
use App\Poceada;
use App\PoceadaPlus;
use App\Primera;
use App\PrimeraDetail;
use App\Providers\HelperServiceProvider;
use App\Quini6;
use App\Vespertina;
use App\VespertinaDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends DrawController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function primera()
    {
        $this->getPrimera();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function matutina()
    {
        $this->getMatutina();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function vespertina()
    {
        $this->getVespertina();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function nocturna()
    {
        $this->getNocturna();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function dollar()
    {
        app('App\Http\Controllers\DollarController')->getDollarExchange();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function createSchema()
    {
        return view('draws.create_schema');
    }

    public function poceada()
    {
        $this->getPoceada();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function poceadaPlus()
    {
        $this->getPoceadaPlus();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function quini()
    {
        $this->getQuini6();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function loto()
    {
        $this->getLoto();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function brinco()
    {
        $this->getBrinco();
        return redirect()->route('index')->with('message', 'La operación se realizó correctamente');
    }

    public function generateSchema($date = "")
    {
        if (empty($date)) {
            $current_date = Carbon::now('America/Argentina/Buenos_Aires');
            $current_date->setTime(0, 0, 0);
            $date = Carbon::createFromFormat('Y-m-d', $current_date->format('Y-m-d'));
            $date->setTime(0, 0, 0);
        }
        Draw::where('date', $date)->delete();
        Primera::where('date', $date)->delete();
        Matutina::where('date', $date)->delete();
        Vespertina::where('date', $date)->delete();
        Nocturna::where('date', $date)->delete();

        $cities = array();
        $cities[] = 'Ciudad';
        $cities[] = 'Provincia';
        $cities[] = 'Santa Fe';
        $cities[] = 'Montevideo';
        $cities[] = 'Entre Ríos';
        $cities[] = 'Mendoza';
        $cities[] = 'Córdoba';
        $cities[] = 'Corrientes';
        $cities[] = 'Chaco';
        $cities[] = 'Santiago';
        $cities[] = 'Neuquén';
        $cities[] = 'San Luis';
        $cities[] = 'Salta';
        $cities[] = 'Jujuy';
        $cities[] = 'Tucumán';
        $cities[] = 'Chubut';
        $cities[] = 'Formosa';
        $cities[] = 'Misiones';
        $cities[] = 'Catamarca';
        $cities[] = 'San Juan';
        $cities[] = 'La Rioja';

        $ids = array();
        $ids[] = '25';
        $ids[] = '24';
        $ids[] = '38';
        $ids[] = '23';
        $ids[] = '39';
        $ids[] = '53';
        $ids[] = '28';
        $ids[] = '42';
        $ids[] = '52';
        $ids[] = '48';
        $ids[] = '41';
        $ids[] = '49';
        $ids[] = '51';
        $ids[] = '50';
        $ids[] = '55';
        $ids[] = '56';
        $ids[] = '59';
        $ids[] = '60';
        $ids[] = '61';
        $ids[] = '62';
        $ids[] = '63';

        //new draw
        $draw = new Draw();
        $draw->date = $date;
        $draw->notified_primera = 0;
        $draw->notified_matutina = 0;
        $draw->notified_vespertina = 0;
        $draw->notified_nocturna = 0;
        $draw->save();

        foreach ($cities as $index => $city) {

            $count = Primera::where('id_loteria', $ids[$index])->where('date', $date)->count();
            if ($count == 0) {
                //draws detail
                $primera = new Primera();
                $primera->city = $city;
                $primera->id_loteria = $ids[$index];
                $primera->id_draw = $draw->_id;
                $primera->value = '----';
                $primera->date = $date;
                $primera->save();

                for ($i = 1; $i <= 20; $i++) {
                    $primera_detail = new PrimeraDetail();
                    $primera_detail->number = $i;
                    $primera_detail->value = '';
                    $primera->numbers()->save($primera_detail);
                }
            }

            $count = Matutina::where('id_loteria', $ids[$index])->where('date', $date)->count();
            if ($count == 0) {
                $matutina = new Matutina();
                $matutina->city = $city;
                $matutina->id_loteria = $ids[$index];
                $matutina->id_draw = $draw->_id;
                $matutina->value = '----';
                $matutina->date = $date;
                $matutina->save();

                for ($i = 1; $i <= 20; $i++) {
                    $matutina_detail = new MatutinaDetail();
                    $matutina_detail->number = $i;
                    $matutina_detail->value = '';
                    $matutina->numbers()->save($matutina_detail);
                }
            }

            $count = Vespertina::where('id_loteria', $ids[$index])->where('date', $date)->count();
            if ($count == 0) {
                $vespertina = new Vespertina();
                $vespertina->city = $city;
                $vespertina->id_loteria = $ids[$index];
                $vespertina->id_draw = $draw->_id;
                $vespertina->value = '----';
                $vespertina->date = $date;
                $vespertina->save();

                for ($i = 1; $i <= 20; $i++) {
                    $vespertina_detail = new VespertinaDetail();
                    $vespertina_detail->number = $i;
                    $vespertina_detail->value = '';
                    $vespertina->numbers()->save($vespertina_detail);
                }
            }

            $count = Nocturna::where('id_loteria', $ids[$index])->where('date', $date)->count();
            if ($count == 0) {
                $nocturna = new Nocturna();
                $nocturna->city = $city;
                $nocturna->id_loteria = $ids[$index];
                $nocturna->id_draw = $draw->_id;
                $nocturna->value = '----';
                $nocturna->date = $date;
                $nocturna->save();

                for ($i = 1; $i <= 20; $i++) {
                    $nocturna_detail = new NocturnaDetail();
                    $nocturna_detail->number = $i;
                    $nocturna_detail->value = '';
                    $nocturna->numbers()->save($nocturna_detail);
                }
            }
        }
    }

    public function storeSchema(Request $request)
    {
        $this->validate($request, [
            'date' => 'required'
        ]);

        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        self::generateSchema($date);
        return back()->with('message', 'El esquema se creó correctamente');
    }

    public function editPrimera()
    {
        return view('draws.primera');
    }

    public function detailPrimera(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $draw = Draw::where('date', $date);
        if ($draw->count() > 0) {
            $draw = $draw->first();
            $primeras = Primera::where('id_draw', $draw->_id)->get();
            return view('draws.primera_detail', compact('primeras', 'draw'));
        } else {
            return '<p><div class="text-danger">No hay registros para mostrar</div></p>';
        }
    }

    public function editNumbersPrimera($id_primera)
    {
        $numbers = array();
        $primera = Primera::find($id_primera);
        if (!empty($primera)) {
            try {
                $numbers = $primera->numbers;
                $array_numbers = array();
                foreach ($numbers as $number) {
                    $detail = new PrimeraDetail();
                    $detail->number = $number->number;
                    $detail->value = $number->value;
                    $array_numbers[] = $detail;
                }
                usort($array_numbers, function ($a, $b) {
                    return intval($a->number) > intval($b->number);
                });
                $numbers = $array_numbers;
            } catch (\Exception $exception) {
            }
        }
        return view('draws.primera_numbers', compact('primera', 'numbers'));
    }

    public function storeNumbersPrimera(Request $request)
    {
        $this->validate($request, [
            'id_primera' => 'required',
            'numbers' => 'required',
            'values' => 'required'
        ]);
        $primera = Primera::find($request->post('id_primera'));
        if (!empty($primera)) {
            $numbers = $request->post('numbers');
            $values = $request->post('values');
            foreach ($numbers as $index => $number) {
                $value_number = intval($number);
                $value_index = $value_number - 1;
                $detail = $primera->numbers()->where('number', $value_number);
                if ($number == "1") {
                    if (empty(trim($values[$index]))) {
                        $primera->value = '----';
                        $primera->meaning = '';
                        $primera->meaning_number = '';
                        $primera->meaning_image = '';
                    } else {
                        $primera->value = $values[$index];
                    }
                    $short_number = substr($values[$index], -2);
                    $meaning = Meaning::where('number', $short_number)->first();
                    if ($short_number == '--' || empty($meaning)) {
                        $primera->meaning = '';
                        $primera->meaning_number = '';
                        $primera->meaning_image = '';
                    } else {
                        if (!empty($meaning)) {
                            $primera->meaning = $meaning->meaning;
                            $primera->meaning_number = $meaning->number;
                            $primera->meaning_image = $meaning->image;
                        }
                    }
                    $primera->save();
                }
                $detail[$value_index]->value = $values[$index];
                $detail[$value_index]->save();
            }
            $array_ids = array();
            $id_results = Primera::select('_id')->where('id_draw', $primera->id_draw)->get();
            foreach ($id_results as $result) {
                $array_ids[] = $result->_id;
            }
            $ids = implode(',', $array_ids);
            \GeneralHelper::instance()->sendIdsToSubscription($ids, 'primera');
            return redirect()->route('draws.primera')->with('message', 'Los números se actualizaron correctamente');
        }
    }

    public function storeDetailPrimera(Request $request)
    {
        
        $this->validate($request, [
            'id_draw' => 'required',
            'values' => 'required',
            'cities' => 'required'
        ]);
        $cities = $request->post('cities');
        $values = $request->post('values');
        $array_ids_modified = array();
        foreach ($cities as $index => $city) {
            $primera = Primera::find($city);
            $primera->value = $values[$index];
            $short_number = substr($values[$index], -2);
            $meaning = Meaning::where('number', $short_number)->first();
            if ($short_number == '--' || empty($meaning)) {
                $primera->meaning = '';
                $primera->meaning_number = '';
                $primera->meaning_image = '';
            } else {
                if (!empty($meaning)) {
                    $primera->meaning = $meaning->meaning;
                    $primera->meaning_number = $meaning->number;
                    $primera->meaning_image = $meaning->image;
                }
            }
            $primera->save();
            $array_ids_modified[] = $primera->_id;
        }
        \GeneralHelper::instance()->sendIdsToSubscription(implode(',', $array_ids_modified), 'primera');
        return redirect()->route('draws.primera')->with('message', 'Los valores se actualizaron correctamente');
    }

    public function editMatutina()
    {
        return view('draws.matutina');
    }

    public function detailMatutina(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $draw = Draw::where('date', $date);
        if ($draw->count() > 0) {
            $draw = $draw->first();
            $matutinas = Matutina::where('id_draw', $draw->_id)->get();
            return view('draws.matutina_detail', compact('matutinas', 'draw'));
        } else {
            return '<p><div class="text-danger">No hay registros para mostrar</div></p>';
        }
    }

    public function editNumbersMatutina($id_matutina)
    {
        $numbers = array();
        $matutina = Matutina::find($id_matutina);
        if (!empty($matutina)) {
            try {
                $numbers = $matutina->numbers;
                $array_numbers = array();
                foreach ($numbers as $number) {
                    $detail = new MatutinaDetail();
                    $detail->number = $number->number;
                    $detail->value = $number->value;
                    $array_numbers[] = $detail;
                }
                usort($array_numbers, function ($a, $b) {
                    return intval($a->number) > intval($b->number);
                });
                $numbers = $array_numbers;
            } catch (\Exception $exception) {
            }
        }
        return view('draws.matutina_numbers', compact('matutina', 'numbers'));
    }

    public function storeNumbersMatutina(Request $request)
    {
        $this->validate($request, [
            'id_matutina' => 'required',
            'numbers' => 'required',
            'values' => 'required'
        ]);
        $matutina = Matutina::find($request->post('id_matutina'));
        if (!empty($matutina)) {
            $numbers = $request->post('numbers');
            $values = $request->post('values');
            foreach ($numbers as $index => $number) {
                $value_number = intval($number);
                $value_index = $value_number - 1;
                $detail = $matutina->numbers()->where('number', $value_number);
                if ($number == "1") {
                    if (empty(trim($values[$index]))) {
                        $matutina->value = '----';
                    } else {
                        $matutina->value = $values[$index];
                    }
                    $short_number = substr($values[$index], -2);
                    $meaning = Meaning::where('number', $short_number)->first();
                    if ($short_number == '--' || empty($meaning)) {
                        $matutina->meaning = '';
                        $matutina->meaning_number = '';
                        $matutina->meaning_image = '';
                    } else {
                        if (!empty($meaning)) {
                            $matutina->meaning = $meaning->meaning;
                            $matutina->meaning_number = $meaning->number;
                            $matutina->meaning_image = $meaning->image;
                        }
                    }
                    $matutina->save();
                }
                $detail[$value_index]->value = $values[$index];
                $detail[$value_index]->save();
            }
            $array_ids = array();
            $id_results = Matutina::select('_id')->where('id_draw', $matutina->id_draw)->get();
            foreach ($id_results as $result) {
                $array_ids[] = $result->_id;
            }
            $ids = implode(',', $array_ids);
            \GeneralHelper::instance()->sendIdsToSubscription($ids, 'matutina');
            return redirect()->route('draws.matutina')->with('message', 'Los números se actualizaron correctamente');
        }
    }

    public function storeDetailMatutina(Request $request)
    {
        $this->validate($request, [
            'id_draw' => 'required',
            'values' => 'required',
            'cities' => 'required'
        ]);
        $cities = $request->post('cities');
        $values = $request->post('values');
        $array_ids_modified = array();
        foreach ($cities as $index => $city) {
            $matutina = Matutina::find($city);
            $matutina->value = $values[$index];
            $short_number = substr($values[$index], -2);
            $meaning = Meaning::where('number', $short_number)->first();
            if ($short_number == '--' || empty($meaning)) {
                $matutina->meaning = '';
                $matutina->meaning_number = '';
                $matutina->meaning_image = '';
            } else {
                if (!empty($meaning)) {
                    $matutina->meaning = $meaning->meaning;
                    $matutina->meaning_number = $meaning->number;
                    $matutina->meaning_image = $meaning->image;
                }
            }
            $matutina->save();
            $array_ids_modified[] = $matutina->_id;
        }
        \GeneralHelper::instance()->sendIdsToSubscription(implode(',', $array_ids_modified), 'matutina');
        return redirect()->route('draws.matutina')->with('message', 'Los valores se actualizaron correctamente');
    }

    public function editVespertina()
    {
        return view('draws.vespertina');
    }

    public function detailVespertina(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $draw = Draw::where('date', $date);
        if ($draw->count() > 0) {
            $draw = $draw->first();
            $vespertinas = Vespertina::where('id_draw', $draw->_id)->get();
            return view('draws.vespertina_detail', compact('vespertinas', 'draw'));
        } else {
            return '<p><div class="text-danger">No hay registros para mostrar</div></p>';
        }
    }

    public function editNumbersVespertina($id_vespertina)
    {
        $numbers = array();
        $vespertina = Vespertina::find($id_vespertina);
        if (!empty($vespertina)) {
            try {
                $numbers = $vespertina->numbers;
                $array_numbers = array();
                foreach ($numbers as $number) {
                    $detail = new VespertinaDetail();
                    $detail->number = $number->number;
                    $detail->value = $number->value;
                    $array_numbers[] = $detail;
                }
                usort($array_numbers, function ($a, $b) {
                    return intval($a->number) > intval($b->number);
                });
                $numbers = $array_numbers;
            } catch (\Exception $exception) {
            }
        }
        return view('draws.vespertina_numbers', compact('vespertina', 'numbers'));
    }

    public function storeNumbersVespertina(Request $request)
    {
        $this->validate($request, [
            'id_vespertina' => 'required',
            'numbers' => 'required',
            'values' => 'required'
        ]);
        $vespertina = Vespertina::find($request->post('id_vespertina'));
        if (!empty($vespertina)) {
            $numbers = $request->post('numbers');
            $values = $request->post('values');
            foreach ($numbers as $index => $number) {
                $value_number = intval($number);
                $value_index = $value_number - 1;
                $detail = $vespertina->numbers()->where('number', $value_number);
                if ($number == "1") {
                    if (empty($values[$index])) {
                        $vespertina->value = '----';
                    } else {
                        $vespertina->value = $values[$index];
                    }
                    $short_number = substr($values[$index], -2);
                    $meaning = Meaning::where('number', $short_number)->first();
                    if ($short_number == '--' || empty($meaning)) {
                        $vespertina->meaning = '';
                        $vespertina->meaning_number = '';
                        $vespertina->meaning_image = '';
                    } else {
                        if (!empty($meaning)) {
                            $vespertina->meaning = $meaning->meaning;
                            $vespertina->meaning_number = $meaning->number;
                            $vespertina->meaning_image = $meaning->image;
                        }
                    }
                    $vespertina->save();
                }
                $detail[$value_index]->value = $values[$index];
                $detail[$value_index]->save();
            }
            $array_ids = array();
            $id_results = Vespertina::select('_id')->where('id_draw', $vespertina->id_draw)->get();
            foreach ($id_results as $result) {
                $array_ids[] = $result->_id;
            }
            $ids = implode(',', $array_ids);
            \GeneralHelper::instance()->sendIdsToSubscription($ids, 'vespertina');
            return redirect()->route('draws.vespertina')->with('message', 'Los números se actualizaron correctamente');
        }
    }

    public function storeDetailVespertina(Request $request)
    {
        $this->validate($request, [
            'id_draw' => 'required',
            'values' => 'required',
            'cities' => 'required'
        ]);
        $cities = $request->post('cities');
        $values = $request->post('values');
        $array_ids_modified = array();
        foreach ($cities as $index => $city) {
            $vespertina = Vespertina::find($city);
            $vespertina->value = $values[$index];
            $short_number = substr($values[$index], -2);
            $meaning = Meaning::where('number', $short_number)->first();
            if ($short_number == '--' || empty($meaning)) {
                $vespertina->meaning = '';
                $vespertina->meaning_number = '';
                $vespertina->meaning_image = '';
            } else {
                if (!empty($meaning)) {
                    $vespertina->meaning = $meaning->meaning;
                    $vespertina->meaning_number = $meaning->number;
                    $vespertina->meaning_image = $meaning->image;
                }
            }
            $vespertina->save();
            $array_ids_modified[] = $vespertina->_id;
        }
        \GeneralHelper::instance()->sendIdsToSubscription(implode(',', $array_ids_modified), 'vespertina');
        return redirect()->route('draws.vespertina')->with('message', 'Los valores se actualizaron correctamente');
    }

    public function editNocturna()
    {
        return view('draws.nocturna');
    }

    public function detailNocturna(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $draw = Draw::where('date', $date);
        if ($draw->count() > 0) {
            $draw = $draw->first();
            $nocturnas = Nocturna::where('id_draw', $draw->_id)->get();
            return view('draws.nocturna_detail', compact('nocturnas', 'draw'));
        } else {
            return '<p><div class="text-danger">No hay registros para mostrar</div></p>';
        }
    }

    public function editNumbersNocturna($id_nocturna)
    {
        $numbers = array();
        $nocturna = Nocturna::find($id_nocturna);
        if (!empty($nocturna)) {
            try {
                $numbers = $nocturna->numbers;
                $array_numbers = array();
                foreach ($numbers as $number) {
                    $detail = new NocturnaDetail();
                    $detail->number = $number->number;
                    $detail->value = $number->value;
                    $array_numbers[] = $detail;
                }
                usort($array_numbers, function ($a, $b) {
                    return intval($a->number) > intval($b->number);
                });
                $numbers = $array_numbers;
            } catch (\Exception $exception) {
            }
        }
        return view('draws.nocturna_numbers', compact('nocturna', 'numbers'));
    }

    public function storeNumbersNocturna(Request $request)
    {
        $this->validate($request, [
            'id_nocturna' => 'required',
            'numbers' => 'required',
            'values' => 'required'
        ]);
        $nocturna = Nocturna::find($request->post('id_nocturna'));
        if (!empty($nocturna)) {
            $numbers = $request->post('numbers');
            $values = $request->post('values');
            foreach ($numbers as $index => $number) {
                $value_number = intval($number);
                $value_index = $value_number - 1;
                $detail = $nocturna->numbers()->where('number', $value_number);
                if ($number == "1") {
                    if (empty(trim($values[$index]))) {
                        $nocturna->value = '----';
                    } else {
                        $nocturna->value = $values[$index];
                    }
                    $short_number = substr($values[$index], -2);
                    $meaning = Meaning::where('number', $short_number)->first();
                    if ($short_number == '--' || empty($meaning)) {
                        $nocturna->meaning = '';
                        $nocturna->meaning_number = '';
                        $nocturna->meaning_image = '';
                    } else {
                        if (!empty($meaning)) {
                            $nocturna->meaning = $meaning->meaning;
                            $nocturna->meaning_number = $meaning->number;
                            $nocturna->meaning_image = $meaning->image;
                        }
                    }
                    $nocturna->save();
                }
                $detail[$value_index]->value = $values[$index];
                $detail[$value_index]->save();
            }
            $array_ids = array();
            $id_results = Nocturna::select('_id')->where('id_draw', $nocturna->id_draw)->get();
            foreach ($id_results as $result) {
                $array_ids[] = $result->_id;
            }
            $ids = implode(',', $array_ids);
            \GeneralHelper::instance()->sendIdsToSubscription($ids, 'nocturna');
            return redirect()->route('draws.nocturna')->with('message', 'Los números se actualizaron correctamente');
        }
    }

    public function storeDetailNocturna(Request $request)
    {
        $this->validate($request, [
            'id_draw' => 'required',
            'values' => 'required',
            'cities' => 'required'
        ]);
        $cities = $request->post('cities');
        $values = $request->post('values');
        $array_ids_modified = array();
        foreach ($cities as $index => $city) {
            $nocturna = Nocturna::find($city);
            $nocturna->value = $values[$index];
            $short_number = substr($values[$index], -2);
            $meaning = Meaning::where('number', $short_number)->first();
            if ($short_number == '--' || empty($meaning)) {
                $nocturna->meaning = '';
                $nocturna->meaning_number = '';
                $nocturna->meaning_image = '';
            } else {
                if (!empty($meaning)) {
                    $nocturna->meaning = $meaning->meaning;
                    $nocturna->meaning_number = $meaning->number;
                    $nocturna->meaning_image = $meaning->image;
                }
            }
            $nocturna->save();
            $array_ids_modified[] = $nocturna->_id;
        }
        \GeneralHelper::instance()->sendIdsToSubscription(implode(',', $array_ids_modified), 'nocturna');
        return redirect()->route('draws.nocturna')->with('message', 'Los valores se actualizaron correctamente');
    }

    public function editPoceada()
    {
        return view('draws.poceada');
    }

    public function createDetailPoceada(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $poceada = Poceada::where('date', $date)->get();
        if ($poceada->count() == 0) {
            $poceada = new Poceada();
            $poceada->date = $date;
            $poceada->draw_number = "";
            $poceada->numbers = array();
            $poceada->prizes = array();
            $poceada->notified = 0;
            $poceada->save();
        }
    }

    public function deleteDetailPoceada(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        Poceada::where('date', $date)->delete();
        return '<p><div class="text-danger">El registro se eliminó correctamente</div></p>';
    }

    public function detailPoceada(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $poceada = Poceada::where('date', $date)->get();
        if ($poceada->count() > 0) {
            $poceada = $poceada->first();
            return view('draws.poceada_detail', compact('poceada'));
        } else {
            return '<p><div class="text-danger">No hay registros para mostrar</div></p>';
        }
    }

    public function storeDetailPoceada(Request $request)
    {
        $this->validate($request, [
            'id_poceada' => 'required',
        ]);
        $numbers = $request->post('numbers');
        $hits = $request->post('hits');
        $winners = $request->post('winners');
        $prizes = $request->post('prizes');
        $id_poceada = $request->post('id_poceada');
        $poceada = Poceada::find($id_poceada);
        $poceada_prizes = array();
        foreach ($hits as $index => $hit) {
            $poceada_prizes[] = ['hits' => $hit, 'winners' => $winners[$index], 'prize' => $prizes[$index]];
        }
        $poceada->numbers = $numbers;
        $poceada->prizes = $poceada_prizes;
        $poceada->save();
        return redirect()->route('draws.poceada')->with('message', 'Los valores se actualizaron correctamente');
    }

    public function editPoceadaPlus()
    {
        return view('draws.poceada_plus');
    }

    public function createDetailPoceadaPlus(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $poceada_plus = PoceadaPlus::where('date', $date)->get();
        if ($poceada_plus->count() == 0) {
            $poceada_plus = new PoceadaPlus();
            $poceada_plus->date = $date;
            $poceada_plus->draw_number = "";
            $poceada_plus->numbers = array();
            $poceada_plus->prizes = array();
            $poceada_plus->notified = 0;
            $poceada_plus->save();
        }
    }

    public function deleteDetailPoceadaPlus(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        PoceadaPlus::where('date', $date)->delete();
        return '<p><div class="text-danger">El registro se eliminó correctamente</div></p>';
    }

    public function detailPoceadaPlus(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $poceada_plus = PoceadaPlus::where('date', $date)->get();
        if ($poceada_plus->count() > 0) {
            $poceada_plus = $poceada_plus->first();
            return view('draws.poceada_plus_detail', compact('poceada_plus'));
        } else {
            return '<p><div class="text-danger">No hay registros para mostrar</div></p>';
        }
    }

    public function storeDetailPoceadaPlus(Request $request)
    {
        $this->validate($request, [
            'id_poceada_plus' => 'required',
        ]);
        $numbers = $request->post('numbers');
        $hits = $request->post('hits');
        $winners = $request->post('winners');
        $prizes = $request->post('prizes');
        $id_poceada_plus = $request->post('id_poceada_plus');
        $poceada_plus = PoceadaPlus::find($id_poceada_plus);
        $poceada_plus_prizes = array();
        foreach ($hits as $index => $hit) {
            $poceada_plus_prizes[] = ['hits' => $hit, 'winners' => $winners[$index], 'prize' => $prizes[$index]];
        }
        $poceada_plus->numbers = $numbers;
        $poceada_plus->prizes = $poceada_plus_prizes;
        $poceada_plus->save();
        return redirect()->route('draws.poceada.plus')->with('message', 'Los valores se actualizaron correctamente');
    }

    public function editBrinco()
    {
        return view('draws.brinco');
    }

    public function createDetailBrinco(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $brinco = Brinco::where('date', $date)->get();
        if ($brinco->count() == 0) {
            $brinco = new Brinco();
            $brinco->date = $date;
            $brinco->draw_number = "";
            $brinco->numbers = array();
            $brinco->prizes = array();
            $brinco->next_prize = "";
            $brinco->notified = 0;
            $brinco->save();
        }
    }

    public function deleteDetailBrinco(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        Brinco::where('date', $date)->delete();
        return '<p><div class="text-danger">El registro se eliminó correctamente</div></p>';
    }

    public function detailBrinco(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $brinco = Brinco::where('date', $date)->get();
        if ($brinco->count() > 0) {
            $brinco = $brinco->first();
            return view('draws.brinco_detail', compact('brinco'));
        } else {
            return '<p><div class="text-danger">No hay registros para mostrar</div></p>';
        }
    }

    public function storeDetailBrinco(Request $request)
    {
        $this->validate($request, [
            'id_brinco' => 'required',
        ]);
        $numbers = $request->post('numbers');
        $hits = $request->post('hits');
        $winners = $request->post('winners');
        $prizes = $request->post('prizes');
        $next_prize = $request->post('next_prize');
        $id_brinco = $request->post('id_brinco');
        $brinco = Brinco::find($id_brinco);
        $brinco_prizes = array();
        foreach ($hits as $index => $hit) {
            $brinco_prizes[] = ['hits' => $hit, 'winners' => $winners[$index], 'prize' => $prizes[$index]];
        }
        $brinco->numbers = $numbers;
        $brinco->prizes = $brinco_prizes;
        $brinco->next_prize = $next_prize;
        $brinco->save();
        return redirect()->route('draws.brinco')->with('message', 'Los valores se actualizaron correctamente');
    }

    public function editQuini()
    {
        return view('draws.quini');
    }

    public function createDetailQuini(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $quini6 = Quini6::where('date', $date)->get();
        if ($quini6->count() == 0) {
            $quini6 = new Quini6();
            $quini6->date = $date;
            $quini6->draw_number = "";
            $quini6->tradicional_numbers = array();
            $quini6->tradicional_prizes = array();
            $quini6->segunda_vuelta_numbers = array();
            $quini6->segunda_vuelta_prizes = array();
            $quini6->revancha_numbers = array();
            $quini6->revancha_prizes = array('winners' => '0', 'prize' => '');
            $quini6->siempre_sale_numbers = array();
            $quini6->siempre_sale_prizes = array('winners' => '0', 'prize' => '');
            $quini6->premio_extra = '';
            $quini6->premio_extra_prizes = array('hits' => '6', 'winners' => '0', 'prize' => '');
            $quini6->prize_next_move = '';
            $quini6->notified = 0;
            $quini6->save();
        }
    }

    public function deleteDetailQuini(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        Quini6::where('date', $date)->delete();
        return '<p><div class="text-danger">El registro se eliminó correctamente</div></p>';
    }

    public function detailQuini(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $quini = Quini6::where('date', $date)->get();
        if ($quini->count() > 0) {
            $quini = $quini->first();
            return view('draws.quini_detail', compact('quini'));
        } else {
            return '<p><div class="text-danger">No hay registros para mostrar</div></p>';
        }
    }

    public function storeDetailQuini(Request $request)
    {
        $this->validate($request, [
            'id_quini' => 'required',
        ]);
        $tradicional_numbers = $request->post('tradicional_numbers');
        $tradicional_hits = $request->post('tradicional_hits');
        $tradicional_winners = $request->post('tradicional_winners');
        $tradicional_prizes = $request->post('tradicional_prizes');

        $segunda_vuelta_numbers = $request->post('segunda_vuelta_numbers');
        $segunda_vuelta_hits = $request->post('segunda_vuelta_hits');
        $segunda_vuelta_winners = $request->post('segunda_vuelta_winners');
        $segunda_vuelta_prizes = $request->post('segunda_vuelta_prizes');

        $revancha_numbers = $request->post('revancha_numbers');
        $revancha_hits = $request->post('revancha_hits');
        $revancha_winners = $request->post('revancha_winners');
        $revancha_prizes = $request->post('revancha_prizes');

        $siempre_sale_numbers = $request->post('siempre_sale_numbers');
        $siempre_sale_hits = $request->post('siempre_sale_hits');
        $siempre_sale_winners = $request->post('siempre_sale_winners');
        $siempre_sale_prizes = $request->post('siempre_sale_prizes');

        $premio_extra = $request->post('premio_extra');
        $premio_extra_hits = $request->post('premio_extra_hits');
        $premio_extra_winners = $request->post('premio_extra_winners');
        $premio_extra_prizes = $request->post('premio_extra_prizes');


        $prize_next_move = $request->post('prize_next_move');
        $id_quini = $request->post('id_quini');
        $quini6 = Quini6::find($id_quini);
        $array_tradicional_prizes = array();
        $array_segunda_vuelta_prizes = array();
        foreach ($tradicional_hits as $index => $hit) {
            $array_tradicional_prizes[] = ['hits' => $hit, 'winners' => $tradicional_winners[$index], 'prize' => $tradicional_prizes[$index]];
        }

        foreach ($segunda_vuelta_hits as $index => $hit) {
            $array_segunda_vuelta_prizes[] = ['hits' => $hit, 'winners' => $segunda_vuelta_winners[$index], 'prize' => $segunda_vuelta_prizes[$index]];
        }

        $array_revancha_prizes = ['winners' => $revancha_winners, 'prize' => $revancha_prizes];
        $array_siempre_sale_prizes = ['winners' => $siempre_sale_winners, 'prize' => $siempre_sale_prizes];
        $array_premio_extra_prizes = ['hits' => $premio_extra_hits, 'winners' => $premio_extra_winners, 'prize' => $premio_extra_prizes];


        $quini6->tradicional_numbers = $tradicional_numbers;
        $quini6->tradicional_prizes = $array_tradicional_prizes;
        $quini6->segunda_vuelta_numbers = $segunda_vuelta_numbers;
        $quini6->segunda_vuelta_prizes = $array_segunda_vuelta_prizes;
        $quini6->revancha_numbers = $revancha_numbers;
        $quini6->revancha_prizes = $array_revancha_prizes;
        $quini6->siempre_sale_numbers = $siempre_sale_numbers;
        $quini6->siempre_sale_prizes = $array_siempre_sale_prizes;
        $quini6->premio_extra = $premio_extra;
        $quini6->premio_extra_prizes = $array_premio_extra_prizes;
        $quini6->prize_next_move = $prize_next_move;
        $quini6->notified = 0;
        $quini6->save();

        return redirect()->route('draws.quini')->with('message', 'Los valores se actualizaron correctamente');
    }


    public function editLoto()
    {
        return view('draws.loto');
    }

    public function createDetailLoto(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $loto = Loto::where('date', $date)->get();
        if ($loto->count() == 0) {
            $loto = new Loto();
            $loto->date = $date;
            $loto->draw_number = "";
            $loto->tradicional_numbers = array();
            $loto->jackpot1_tradicional = '';
            $loto->jackpot2_tradicional = '';
            $loto->tradicional_prizes = array();
            $loto->desquite_numbers = array();
            $loto->jackpot1_desquite = '';
            $loto->jackpot2_desquite = '';
            $loto->desquite_prizes = array();
            $loto->sale_sale_numbers = array();
            $loto->jackpot1_sale_sale = '';
            $loto->jackpot2_sale_sale = '';
            $loto->jackpot3_sale_sale = '';
            $loto->jackpot4_sale_sale = '';
            $loto->sale_sale_prizes = array('winners' => '', 'prize' => '');
            $loto->sale_sale_duplicador = '';
            $loto->prize_next_move = '';
            $loto->notified = 0;
            $loto->save();
        }
    }

    public function deleteDetailLoto(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        Loto::where('date', $date)->delete();
        return '<p><div class="text-danger">El registro se eliminó correctamente</div></p>';
    }

    public function detailLoto(Request $request)
    {
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $loto = Loto::where('date', $date)->get();
        if ($loto->count() > 0) {
            $loto = $loto->first();
            return view('draws.loto_detail', compact('loto'));
        } else {
            return '<p><div class="text-danger">No hay registros para mostrar</div></p>';
        }
    }

    public function storeDetailLoto(Request $request)
    {
        $this->validate($request, [
            'id_loto' => 'required',
        ]);
        $tradicional_numbers = $request->post('tradicional_numbers');
        foreach ($tradicional_numbers as $index => $tradicional_number) {
            $tradicional_numbers[$index] = trim($tradicional_number);
        }
        $jackpot1_tradicional = trim($request->post('jackpot1_tradicional'));
        $jackpot2_tradicional = trim($request->post('jackpot2_tradicional'));
        $tradicional_labels = $request->post('tradicional_labels');
        $tradicional_winners = $request->post('tradicional_winners');
        $tradicional_prizes = $request->post('tradicional_prizes');
        $array_tradicional_prizes = array();
        foreach ($tradicional_labels as $index => $label) {
            $array_tradicional_prizes[] = ['label' => trim($label), 'winners' => trim($tradicional_winners[$index]), 'prize' => trim($tradicional_prizes[$index])];
        }

        $desquite_numbers = $request->post('desquite_numbers');
        foreach ($desquite_numbers as $index => $desquite_number) {
            $desquite_numbers[$index] = trim($desquite_number);
        }
        $jackpot1_desquite = trim($request->post('jackpot1_desquite'));
        $jackpot2_desquite = trim($request->post('jackpot2_desquite'));
        $desquite_labels = $request->post('desquite_labels');
        $desquite_winners = $request->post('desquite_winners');
        $desquite_prizes = $request->post('desquite_prizes');
        $array_desquite_prizes = array();
        foreach ($desquite_labels as $index => $label) {
            $array_desquite_prizes[] = ['label' => trim($label), 'winners' => trim($desquite_winners[$index]), 'prize' => trim($desquite_prizes[$index])];
        }

        $sale_sale_numbers = $request->post('sale_sale_numbers');
        foreach ($sale_sale_numbers as $index => $sale_sale_number) {
            $sale_sale_numbers[$index] = trim($sale_sale_number);
        }
        $jackpot1_sale_sale = trim($request->post('jackpot1_sale_sale'));
        $jackpot2_sale_sale = trim($request->post('jackpot2_sale_sale'));
        $jackpot3_sale_sale = trim($request->post('jackpot3_sale_sale'));
        $jackpot4_sale_sale = trim($request->post('jackpot4_sale_sale'));

        $array_sale_sale_prizes = ['winners' => trim($request->post('sale_sale_winners')), 'prize' => trim($request->post('sale_sale_prize'))];

        $sale_sale_duplicador = trim($request->post('sale_sale_duplicador'));
        $prize_next_move = trim($request->post('prize_next_move'));

        $id_loto = $request->post('id_loto');
        $loto = Loto::find($id_loto);
        $loto->tradicional_numbers = $tradicional_numbers;
        $loto->jackpot1_tradicional = $jackpot1_tradicional;
        $loto->jackpot2_tradicional = $jackpot2_tradicional;
        $loto->tradicional_prizes = $array_tradicional_prizes;
        $loto->desquite_numbers = $desquite_numbers;
        $loto->jackpot1_desquite = $jackpot1_desquite;
        $loto->jackpot2_desquite = $jackpot2_desquite;
        $loto->desquite_prizes = $array_desquite_prizes;
        $loto->sale_sale_numbers = $sale_sale_numbers;
        $loto->jackpot1_sale_sale = $jackpot1_sale_sale;
        $loto->jackpot2_sale_sale = $jackpot2_sale_sale;
        $loto->jackpot3_sale_sale = $jackpot3_sale_sale;
        $loto->jackpot4_sale_sale = $jackpot4_sale_sale;
        $loto->sale_sale_prizes = $array_sale_sale_prizes;
        $loto->sale_sale_duplicador = $sale_sale_duplicador;
        $loto->prize_next_move = $prize_next_move;
        $loto->save();
        return redirect()->route('draws.loto')->with('message', 'Los valores se actualizaron correctamente');
    }
}