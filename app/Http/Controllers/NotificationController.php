<?php

namespace App\Http\Controllers;

use App\Brinco;
use App\Draw;
use App\Loto;
use App\Matutina;
use App\Nocturna;
use App\Poceada;
use App\PoceadaPlus;
use App\Primera;
use App\Quini6;
use App\Vespertina;
use Berkayk\OneSignal\OneSignalFacade as OneSignal;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;

class NotificationController extends Controller
{
    var $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");

    public function sendNotification($current_draw)
    {
        $date = Carbon::now('America/Argentina/Buenos_Aires');
        $date = Carbon::createFromFormat('Y-m-d', $date->toDateString());
        $date->setTime(0, 0, 0);
        $draws = Draw::where('date', $date)->get();
        $id_ciudad = '25';
        $id_provincia = '24';
        if ($draws->count() > 0) {
            $draw = $draws->first();
            switch ($current_draw) {
                case 'primera':
                    if ($draw->notified_primera == 1) {
                        return;
                    }
                    $has_values = true;
                    $primera_ciudad = Primera::where('id_draw', $draw->_id)->where('id_loteria', $id_ciudad)->first();
                    $primera_provincia = Primera::where('id_draw', $draw->_id)->where('id_loteria', $id_provincia)->first();
                    if (empty($primera_ciudad) || empty($primera_provincia) || $primera_ciudad->value == '----' || $primera_provincia->value == '----') {
                        $has_values = false;
                    }
                    if ($has_values) {
                        $message = 'PRIMERA! ' . $primera_ciudad->meaning . ' y ' . $primera_provincia->meaning . ' ' . $primera_ciudad->city . ' ' . $primera_ciudad->value . ' - ' . $primera_provincia->city . ' ' . $primera_provincia->value;
                        $this->buildImageAndNotificationForDraw($message, $current_draw, $primera_ciudad, $primera_provincia);
                        //OneSignal::sendNotificationUsingTags($message, array(["field" => "tag", 'key' => 'primera', "relation" => "=", "value" => "1"]), $url = null, $data = array('message' => $message), $buttons = null, $schedule = null);
                        $draw->notified_primera = 1;
                        $draw->save();
                    }
                    break;
                case 'matutina':
                    if ($draw->notified_matutina == 1) {
                        return;
                    }
                    $has_values = true;
                    $matutina_ciudad = Matutina::where('id_draw', $draw->_id)->where('id_loteria', $id_ciudad)->first();
                    $matutina_provincia = Matutina::where('id_draw', $draw->_id)->where('id_loteria', $id_provincia)->first();
                    if (empty($matutina_ciudad) || empty($matutina_provincia) || $matutina_ciudad->value == '----' || $matutina_provincia->value == '----') {
                        $has_values = false;
                    }
                    if ($has_values) {
                        $message = 'MATUTINA! ' . $matutina_ciudad->meaning . ' y ' . $matutina_provincia->meaning . ' ' . $matutina_ciudad->city . ' ' . $matutina_ciudad->value . ' - ' . $matutina_provincia->city . ' ' . $matutina_provincia->value;
                        $this->buildImageAndNotificationForDraw($message, $current_draw, $matutina_ciudad, $matutina_provincia);
                        //OneSignal::sendNotificationUsingTags($message, array(["field" => "tag", 'key' => 'matutina', "relation" => "=", "value" => "1"]), $url = null, $data = array('message' => $message), $buttons = null, $schedule = null);
                        $draw->notified_matutina = 1;
                        $draw->save();
                    }
                    break;
                case 'vespertina':
                    if ($draw->notified_vespertina == 1) {
                        return;
                    }
                    $has_values = true;
                    $vespertina_ciudad = Vespertina::where('id_draw', $draw->_id)->where('id_loteria', $id_ciudad)->first();
                    $vespertina_provincia = Vespertina::where('id_draw', $draw->_id)->where('id_loteria', $id_provincia)->first();
                    if (empty($vespertina_ciudad) || empty($vespertina_provincia) || $vespertina_ciudad->value == '----' || $vespertina_provincia->value == '----') {
                        $has_values = false;
                    }
                    if ($has_values) {
                        $message = 'VESPERTINA! ' . $vespertina_ciudad->meaning . ' y ' . $vespertina_provincia->meaning . ' ' . $vespertina_ciudad->city . ' ' . $vespertina_ciudad->value . ' - ' . $vespertina_provincia->city . ' ' . $vespertina_provincia->value;
                        $this->buildImageAndNotificationForDraw($message, $current_draw, $vespertina_ciudad, $vespertina_provincia);
                        //OneSignal::sendNotificationUsingTags($message, array(["field" => "tag", 'key' => 'vespertina', "relation" => "=", "value" => "1"]), $url = null, $data = array('message' => $message), $buttons = null, $schedule = null);
                        $draw->notified_vespertina = 1;
                        $draw->save();
                    }
                    break;
                case 'nocturna':
                    if ($draw->notified_nocturna == 1) {
                        return;
                    }
                    $has_values = true;
                    $nocturna_ciudad = Nocturna::where('id_draw', $draw->_id)->where('id_loteria', $id_ciudad)->first();
                    $nocturna_provincia = Nocturna::where('id_draw', $draw->_id)->where('id_loteria', $id_provincia)->first();
                    if (empty($nocturna_ciudad) || empty($nocturna_provincia) || $nocturna_ciudad->value == '----' || $nocturna_provincia->value == '----') {
                        $has_values = false;
                    }
                    if ($has_values) {
                        $message = 'NOCTURNA! ' . $nocturna_ciudad->meaning . ' y ' . $nocturna_provincia->meaning . ' ' . $nocturna_ciudad->city . ' ' . $nocturna_ciudad->value . ' - ' . $nocturna_provincia->city . ' ' . $nocturna_provincia->value;
                        $this->buildImageAndNotificationForDraw($message, $current_draw, $nocturna_ciudad, $nocturna_provincia);
                        //OneSignal::sendNotificationUsingTags($message, array(["field" => "tag", 'key' => 'nocturna', "relation" => "=", "value" => "1"]), $url = null, $data = array('message' => $message), $buttons = null, $schedule = null);
                        $draw->notified_nocturna = 1;
                        $draw->save();
                    }
                    break;
            }
        }
    }

    public function buildImageAndNotificationForDraw($message, $draw, $ciudad, $provincia)
    {
        $imageName = 'notifications/' . trim($draw) . '.png';
        $callback = function ($font) {
            $font->file(public_path('fonts/FredokaOne.ttf'));
            $font->size(69);
            $font->color('#2946C9');
        };
        $callback2 = function ($font) {
            $font->file(public_path('fonts/FredokaOne.ttf'));
            $font->size(52);
            $font->color('#FFFFFF');
        };
        $value1 = $ciudad->value;
        $value2 = $provincia->value;

        $image1 = substr($value1, -2, 2);
        $image2 = substr($value2, -2, 2);

        $img = Image::make(public_path('images/quiniela.png'));
        $img->insert('https://quesalio.com/img/emoticons/' . $image1 . '.png', '', 165, 210);
        $img->insert('https://quesalio.com/img/emoticons/' . $image2 . '.png', '', 465, 210);

        $number1 = str_pad($value1, 4, ' ', STR_PAD_BOTH);
        $number2 = str_pad($value2, 4, ' ', STR_PAD_BOTH);
        $img->text($number1, 125, 372, $callback);
        $img->text($number2, 427, 372, $callback);
        $img->text(strtoupper($draw), 215, 70, $callback2);
        $finalImageUrl = dirname(public_path(), 1) . '/' . $imageName;
        //$img->save(public_path($imageName));
        $img->save($finalImageUrl);
        //$imageUrl = url($imageName);
        $imageUrl = 'https://quesalio.com/' . $imageName;
        //$imageUrl = str_replace('http://', 'https://', $imageUrl);
        $params = [];
        $contents = [
            "en" => $message,
            "es" => $message
        ];
        $params['contents'] = $contents;
        $params['big_picture'] = $imageUrl;
        $params['data'] = array('message' => $message, 'type' => '');
        $params['filters'] = array(["field" => "tag", 'key' => $draw, "relation" => "=", "value" => "1"]);


        fcm()
            ->toTopic($draw)
            ->data([
                'image' => $imageUrl,
                'message' => $message,
            ])
            ->send();
        OneSignal::sendNotificationCustom($params);
    }


    public function checkBrinco()
    {
        $brinco = Brinco::where('notified', '!=', 1)
            ->orderBy('date', 'desc')
            ->first();
        $numbers = $brinco->numbers;
        $counter = count($numbers);
        if ($counter == 6) {
            $is_complete = true;
            foreach ($numbers as $number) {
                $n = intval($number);
                if (!is_numeric($n) || ($n == 0 && $number != '0' && $number != '00')) {
                    $is_complete = false;
                }
            }
            if ($is_complete) {
                $format = "Y-m-d";
                $newDate = date_format(date_create($brinco->date), $format);
                $dayOfWeek = $this->dias[date('w', strtotime($newDate))];
                $message = "💰 BRINCO: HOY " . $dayOfWeek . " 🤞 ¡Números ganadores!";
                $this->buildImageAndNotification($numbers, $message, 'brinco');
                //OneSignal::sendNotificationUsingTags($message, array(["field" => "tag", 'key' => 'brinco', "relation" => "=", "value" => "1"]), $url = null, $data = array('message' => $message), $buttons = null, $schedule = null);
                $brinco->notified = 1;
                $brinco->save();
            }
        }
    }

    public function checkPoceada()
    {
        $poceada = Poceada::where('notified', 0)
            ->orderBy('date', 'desc')
            ->first();
        $numbers = $poceada->numbers;
        $counter = count($numbers);
        if ($counter == 20) {
            $is_complete = true;
            foreach ($numbers as $number) {
                $n = intval($number);
                if (!is_numeric($n) || ($n == 0 && $number != '0' && $number != '00')) {
                    $is_complete = false;
                }
            }
            if ($is_complete) {
                $format = "Y-m-d";
                $newDate = date_format(date_create($poceada->date), $format);
                $dayOfWeek = $this->dias[date('w', strtotime($newDate))];
                $message = "💰 POCEADA: HOY " . $dayOfWeek . " 🤞 ¡Números ganadores!";
                $this->buildImageAndNotification($numbers, $message, 'poceada');
                //OneSignal::sendNotificationUsingTags($message, array(["field" => "tag", 'key' => 'poceada', "relation" => "=", "value" => "1"]), $url = null, $data = array('message' => $message), $buttons = null, $schedule = null);
                $poceada->notified = 1;
                $poceada->save();
            }
        }
    }

    public function checkPoceadaPlus()
    {
        $poceada_plus = PoceadaPlus::where('notified', 0)
            ->orderBy('date', 'desc')
            ->first();
        $numbers = $poceada_plus->numbers;
        $counter = count($numbers);
        if ($counter == 20) {
            $is_complete = true;
            foreach ($numbers as $number) {
                $n = intval($number);
                if (!is_numeric($n) || ($n == 0 && $number != '0' && $number != '00')) {
                    $is_complete = false;
                }
            }
            if ($is_complete) {
                $format = "Y-m-d";
                $newDate = date_format(date_create($poceada_plus->date), $format);
                $dayOfWeek = $this->dias[date('w', strtotime($newDate))];
                $message = "💰 POCEADA PLUS: HOY " . $dayOfWeek . " 🤞 ¡Números ganadores!";
                $this->buildImageAndNotification($numbers, $message, 'poceada_plus');
                //OneSignal::sendNotificationUsingTags($message, array(["field" => "tag", 'key' => 'poceada_plus', "relation" => "=", "value" => "1"]), $url = null, $data = array('message' => $message), $buttons = null, $schedule = null);
                $poceada_plus->notified = 1;
                $poceada_plus->save();
            }
        }
    }

    public function checkLoto()
    {
        $loto = Loto::where('notified', 0)
            ->orderBy('date', 'desc')
            ->first();
        $tradicional_numbers = $loto->tradicional_numbers;
        $desquite_numbers = $loto->desquite_numbers;
        $counter_tradicional = count($tradicional_numbers);
        $counter_desquite = count($desquite_numbers);

        if ($counter_tradicional == 6 && $counter_desquite == 6) {
            $is_complete_tradicional = true;
            $is_complete_desquite = true;
            foreach ($tradicional_numbers as $number) {
                $n = intval($number);
                if (!is_numeric($n) || ($n == 0 && $number != '0' && $number != '00')) {
                    $is_complete_tradicional = false;
                }
            }
            foreach ($desquite_numbers as $number) {
                $n = intval($number);
                if (!is_numeric($n) || ($n == 0 && $number != '0' && $number != '00')) {
                    $is_complete_desquite = false;
                }
            }
            if ($is_complete_tradicional && $is_complete_desquite) {
                $format = "Y-m-d";
                $newDate = date_format(date_create($loto->date), $format);
                $dayOfWeek = $this->dias[date('w', strtotime($newDate))];
                $message = "💰 LOTO: HOY " . $dayOfWeek . " 🤞 ¡Números ganadores!";
                $this->buildImageAndNotification(array_merge($tradicional_numbers, $desquite_numbers), $message, 'loto');
                //OneSignal::sendNotificationUsingTags($message, array(["field" => "tag", 'key' => 'loto', "relation" => "=", "value" => "1"]), $url = null, $data = array('message' => $message), $buttons = null, $schedule = null);
                $loto->notified = 1;
                $loto->save();
            }
        }
    }

    public function checkQuini()
    {
        $quini = Quini6::where('notified', 0)
            ->orderBy('date', 'desc')
            ->first();
        $tradicional_numbers = $quini->tradicional_numbers;
        $segunda_vuelta_numbers = $quini->segunda_vuelta_numbers;
        $counter_tradicional = count($tradicional_numbers);
        $counter_segunda_vuelta = count($segunda_vuelta_numbers);

        if ($counter_tradicional == 6 && $counter_segunda_vuelta == 6) {
            $is_complete_tradicional = true;
            $is_complete_segunda_vuelta = true;
            foreach ($tradicional_numbers as $number) {
                $n = intval($number);
                if (!is_numeric($n) || ($n == 0 && $number != '0' && $number != '00')) {
                    $is_complete_tradicional = false;
                }
            }
            foreach ($segunda_vuelta_numbers as $number) {
                $n = intval($number);
                if (!is_numeric($n) || ($n == 0 && $number != '0' && $number != '00')) {
                    $is_complete_segunda_vuelta = false;
                }
            }
            if ($is_complete_tradicional && $is_complete_segunda_vuelta) {
                $format = "Y-m-d";
                $newDate = date_format(date_create($quini->date), $format);
                $dayOfWeek = $this->dias[date('w', strtotime($newDate))];
                $message = "💰 QUINI 6: HOY " . $dayOfWeek . " 🤞 ¡Números ganadores!";
                //OneSignal::sendNotificationUsingTags($message, array(["field" => "tag", 'key' => 'quini', "relation" => "=", "value" => "1"]), $url = null, $data = array('message' => $message), $buttons = null, $schedule = null);
                $this->buildImageAndNotification(array_merge($tradicional_numbers, $segunda_vuelta_numbers), $message, 'quini');
                $quini->notified = 1;
                $quini->save();
            }
        }
    }

    public function buildImageAndNotification($numbers, $message, $type)
    {
        $callback = function ($font) {
            $font->file(public_path('fonts/FredokaOne.ttf'));
            $font->size(48);
            $font->color('#2946C9');
        };
        switch ($type) {
            case 'poceada':
                $imageName = 'notifications/poceada.png';
                $img = Image::make(public_path('images/poceada.png'));
                $array_x = array(60, 167, 274, 379, 485, 592, 60, 167, 274, 379, 485, 592);
                $array_y = array(228, 228, 228, 228, 228, 228, 341, 341, 341, 341, 341, 341);
                foreach ($numbers as $index => $number) {
                    $img->text($number, $array_x[$index], $array_y[$index], $callback);
                    if ($index >= 11) break;
                }
                $img->save(public_path($imageName));
                $imageUrl = url($imageName);
                $params = [];
                $contents = [
                    "en" => $message,
                    "es" => $message
                ];
                $params['contents'] = $contents;
                $params['big_picture'] = $imageUrl;
                $params['data'] = array('message' => $message, 'type' => $type);
                $params['filters'] = array(["field" => "tag", 'key' => $type, "relation" => "=", "value" => "1"]);
                OneSignal::sendNotificationCustom($params);
                break;
            case 'poceada_plus':
                $imageName = 'notifications/poceada_plus.png';
                $img = Image::make(public_path('images/poceadaplus.png'));
                $array_x = array(60, 167, 274, 379, 485, 592, 60, 167, 274, 379, 485, 592);
                $array_y = array(224, 224, 224, 224, 224, 224, 338, 338, 338, 338, 338, 338);
                foreach ($numbers as $index => $number) {
                    $img->text($number, $array_x[$index], $array_y[$index], $callback);
                    if ($index >= 11) break;
                }
                $img->save(public_path($imageName));
                $imageUrl = url($imageName);
                $params = [];
                $contents = [
                    "en" => $message,
                    "es" => $message
                ];
                $params['contents'] = $contents;
                $params['big_picture'] = $imageUrl;
                $params['data'] = array('message' => $message, 'type' => $type);
                $params['filters'] = array(["field" => "tag", 'key' => $type, "relation" => "=", "value" => "1"]);
                OneSignal::sendNotificationCustom($params);
                break;
            case 'brinco':
                $imageName = 'notifications/brinco.png';
                $img = Image::make(public_path('images/brinco.png'));
                $array_x = array(60, 167, 274, 379, 485, 592);
                $array_y = array(224, 224, 224, 224, 224, 224);
                foreach ($numbers as $index => $number) {
                    $img->text($number, $array_x[$index], $array_y[$index], $callback);
                    if ($index >= 6) break;
                }
                $img->save(public_path($imageName));
                $imageUrl = url($imageName);
                $params = [];
                $contents = [
                    "en" => $message,
                    "es" => $message
                ];
                $params['contents'] = $contents;
                $params['big_picture'] = $imageUrl;
                $params['data'] = array('message' => $message, 'type' => $type);
                $params['filters'] = array(["field" => "tag", 'key' => $type, "relation" => "=", "value" => "1"]);
                OneSignal::sendNotificationCustom($params);
                break;
            case 'loto':
                $imageName = 'notifications/loto.png';
                $img = Image::make(public_path('images/loto.png'));
                $array_x = array(60, 167, 274, 379, 485, 592, 60, 167, 274, 379, 485, 592);
                $array_y = array(224, 224, 224, 224, 224, 224, 338, 338, 338, 338, 338, 338);
                foreach ($numbers as $index => $number) {
                    $img->text($number, $array_x[$index], $array_y[$index], $callback);
                    if ($index >= 11) break;
                }
                $img->save(public_path($imageName));
                $imageUrl = url($imageName);
                $params = [];
                $contents = [
                    "en" => $message,
                    "es" => $message
                ];
                $params['contents'] = $contents;
                $params['big_picture'] = $imageUrl;
                $params['data'] = array('message' => $message, 'type' => $type);
                $params['filters'] = array(["field" => "tag", 'key' => $type, "relation" => "=", "value" => "1"]);
                OneSignal::sendNotificationCustom($params);
                break;
            case 'quini':
                $imageName = 'notifications/quini.png';
                $img = Image::make(public_path('images/quini.png'));
                $array_x = array(60, 167, 274, 379, 485, 592, 60, 167, 274, 379, 485, 592);
                $array_y = array(224, 224, 224, 224, 224, 224, 338, 338, 338, 338, 338, 338);
                foreach ($numbers as $index => $number) {
                    $img->text($number, $array_x[$index], $array_y[$index], $callback);
                    if ($index >= 11) break;
                }
                $img->save(public_path($imageName));
                $imageUrl = url($imageName);
                $params = [];
                $contents = [
                    "en" => $message,
                    "es" => $message
                ];
                $params['contents'] = $contents;
                $params['big_picture'] = $imageUrl;
                $params['data'] = array('message' => $message, 'type' => $type);
                $params['filters'] = array(["field" => "tag", 'key' => $type, "relation" => "=", "value" => "1"]);
                OneSignal::sendNotificationCustom($params);
                break;
        }

    }


    public function generateImage()
    {
        $callback = function ($font) {
            $font->file(public_path('fonts/FredokaOne.ttf'));
            $font->size(69);
            $font->color('#2946C9');
        };
        $callback2 = function ($font) {
            $font->file(public_path('fonts/FredokaOne.ttf'));
            $font->size(52);
            $font->color('#FFFFFF');
        };
        $img = Image::make(public_path('images/quiniela.png'));
        $img->insert('https://quesalio.com/img/emoticons/00.png', '', 165, 210);
        $img->insert('https://quesalio.com/img/emoticons/01.png', '', 465, 210);

        $number1 = str_pad('22', 4, ' ', STR_PAD_BOTH);
        $number2 = str_pad('103', 4, ' ', STR_PAD_BOTH);
        $img->text($number1, 125, 372, $callback);
        $img->text($number2, 427, 372, $callback);
        $title = "VESPERTINA";
        $img->text($title, 215, 70, $callback2);
        /*$img->text('10', 274, 224, $callback);
        $img->text('10', 379, 224, $callback);
        $img->text('10', 485, 224, $callback);
        $img->text('10', 592, 224, $callback);
        //second row

        $img->text('10', 60, 338, $callback);
        $img->text('10', 167, 338, $callback);
        $img->text('10', 274, 338, $callback);
        $img->text('10', 379, 338, $callback);
        $img->text('10', 485, 338, $callback);
        $img->text('10', 592, 338, $callback);*/
        $img->save(public_path('test.png'));
        exit;
        $image_url = url('test.png');
        $message = "test from image";

        $userId = "77aa18b9-dd9b-4d16-90f1-06c7ff2b1618";
        $params = [];
        $params['include_player_ids'] = array($userId);
        $contents = [
            "en" => "Some English Message",
            "es" => "Some Turkish Message"
        ];
        $params['contents'] = $contents;
        //$params['delayed_option'] = "timezone"; // Will deliver on user's timezone
        //$params['delivery_time_of_day'] = "12:25PM"; // Delivery time
        $params['big_picture'] = $image_url;
        $params['data'] = array('message' => $message);
        $params['filters'] = array(["field" => "tag", 'key' => 'loto', "relation" => "=", "value" => "1"]);

        //OneSignal::sendNotificationToUser("Some Message", "77aa18b9-dd9b-4d16-90f1-06c7ff2b1618", $url = null, $data = array('message' => $message, 'big_picture' => $image_url), $buttons = null, $schedule = null);
        OneSignal::sendNotificationCustom($params);

        //return $img;
    }
}
