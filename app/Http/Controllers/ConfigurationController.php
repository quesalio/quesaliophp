<?php

namespace App\Http\Controllers;

use App\Configuration;
use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use GuzzleHttp\Client as GuzzleClient;

class ConfigurationController extends Controller
{
    public function create()
    {
        $configuration = Configuration::all();
        if ($configuration->count() > 0) {
            $configuration = $configuration->first();
        }
        return view('configuration.create', compact('configuration'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required'
        ]);
        $configuration = Configuration::all();
        if ($configuration->count() > 0) {
            $configuration = $configuration->first();
        } else {
            $configuration = new Configuration();
        }
        $date = $request->post('date');
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $date->setTime(0, 0, 0);
        $configuration->showNumbers = $request->post('showNumbers') == 'on' ? "1" : "0";
        $configuration->showEphemerides = $request->post('showEphemerides') == 'on' ? "1" : "0";
        $configuration->showRecomendations = $request->post('showRecomendations') == 'on' ? "1" : "0";
        $configuration->date = $date;
        $configuration->save();
        $this->callGraphqlQuery();
        return redirect()->route('configuration.create')->with('message', 'El registro se actualizó correctamente');
    }

    public function startDraw($draw)
    {
        $configuration = Configuration::all();
        if ($configuration->count() > 0) {
            $configuration = $configuration->first();
        } else {
            $configuration = new Configuration();
            $date = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
            $date->setTime(0, 0, 0);
            $configuration->showNumbers = "1";
            $configuration->showEphemerides = "1";
            $configuration->showRecomendations = "1";
            $configuration->date = $date;
        }
        switch ($draw) {
            case 'primera':
                $configuration->blink_message = 'SOOORTEANDO Primera...';
                break;
            case 'matutina':
                $configuration->blink_message = 'SOOORTEANDO Matutina...';
                break;
            case 'vespertina':
                $configuration->blink_message = 'SOOORTEANDO Vespertina...';
                break;
            case 'nocturna':
                $configuration->blink_message = 'SOOORTEANDO Nocturna...';
                break;
        }
        $configuration->blink = "1";
        $configuration->save();
        $this->callGraphqlQuery();
        return redirect()->route('configuration.create')->with('message', 'El sorteo se inició correctamente');
    }

    public function stopDraw()
    {
        $configuration = Configuration::all();
        if ($configuration->count() > 0) {
            $configuration = $configuration->first();
            $configuration->blink = "0";
            $configuration->save();
        }
        $this->callGraphqlQuery();
        return redirect()->route('configuration.create')->with('message', 'El sorteo se detuvo correctamente');
    }

    public function getCurrentDate()
    {
        $date=Carbon::now('America/Argentina/Buenos_Aires');
        $date->setTime(0,0,0);
        $date=$date->format('Y-m-d');

        //$date = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
        //$date->setTime(0, 0, 0);

        $configuration = Configuration::all();
        if ($configuration->count() > 0) {
            $configuration = $configuration->first();
            $configuration->date = $date;
            $configuration->save();
        } else {
            $configuration = new Configuration();
            $configuration->showNumbers = "1";
            $configuration->showEphemerides = "1";
            $configuration->showRecomendations = "1";
            $configuration->date = $date;
        }
    }
    private function callGraphqlQuery(){
        $url = config('general.graphql_subscription_url_for_query');
        $query = 'query{configuration_changed {showNumbers,showEphemerides,showRecomendations,blink,blink_message,date,current_date}}';
        $new_url = $url . urlencode($query);
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $goutteClient->request('GET', $new_url);
    }
}
