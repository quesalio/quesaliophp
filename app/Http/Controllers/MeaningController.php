<?php

namespace App\Http\Controllers;

use App\Meaning;

class MeaningController extends Controller
{


    public function createMeanings()
    {
        $meanings = array(
            array('id' => '1', 'nro' => '00', 'sig' => 'Los huevos', 'img' => '00.png'),
            array('id' => '2', 'nro' => '01', 'sig' => 'El agua', 'img' => '01.png'),
            array('id' => '3', 'nro' => '02', 'sig' => 'El niño', 'img' => '02.png'),
            array('id' => '4', 'nro' => '03', 'sig' => 'San Cono', 'img' => '03.png'),
            array('id' => '5', 'nro' => '04', 'sig' => 'La cama', 'img' => '04.png'),
            array('id' => '6', 'nro' => '05', 'sig' => 'El gato', 'img' => '05.png'),
            array('id' => '7', 'nro' => '06', 'sig' => 'El perro', 'img' => '06.png'),
            array('id' => '8', 'nro' => '07', 'sig' => 'El revolver', 'img' => '07.png'),
            array('id' => '9', 'nro' => '08', 'sig' => 'El incendio', 'img' => '08.png'),
            array('id' => '10', 'nro' => '09', 'sig' => 'El arroyo', 'img' => '09.png'),
            array('id' => '11', 'nro' => '10', 'sig' => 'La leche', 'img' => '10.png'),
            array('id' => '12', 'nro' => '11', 'sig' => 'Palito', 'img' => '11.png'),
            array('id' => '13', 'nro' => '12', 'sig' => 'El Soldado', 'img' => '12.png'),
            array('id' => '14', 'nro' => '13', 'sig' => 'La yeta', 'img' => '13.png'),
            array('id' => '15', 'nro' => '14', 'sig' => 'El borracho', 'img' => '14.png'),
            array('id' => '16', 'nro' => '15', 'sig' => 'La niña bonita', 'img' => '15.png'),
            array('id' => '17', 'nro' => '16', 'sig' => 'La Llave', 'img' => '16.png'),
            array('id' => '18', 'nro' => '17', 'sig' => 'La desgracia', 'img' => '17.png'),
            array('id' => '19', 'nro' => '18', 'sig' => 'La sangre', 'img' => '18.png'),
            array('id' => '20', 'nro' => '19', 'sig' => 'El pescado', 'img' => '19.png'),
            array('id' => '21', 'nro' => '20', 'sig' => 'La fiesta', 'img' => '20.png'),
            array('id' => '22', 'nro' => '21', 'sig' => 'La mujer', 'img' => '21.png'),
            array('id' => '23', 'nro' => '22', 'sig' => 'El loco', 'img' => '22.png'),
            array('id' => '24', 'nro' => '23', 'sig' => 'El cocinero', 'img' => '23.png'),
            array('id' => '25', 'nro' => '24', 'sig' => 'El caballo', 'img' => '24.png'),
            array('id' => '26', 'nro' => '25', 'sig' => 'La gallina', 'img' => '25.png'),
            array('id' => '27', 'nro' => '26', 'sig' => 'La misa', 'img' => '26.png'),
            array('id' => '28', 'nro' => '27', 'sig' => 'El peine', 'img' => '27.png'),
            array('id' => '29', 'nro' => '28', 'sig' => 'Melones', 'img' => '28.png'),
            array('id' => '30', 'nro' => '29', 'sig' => 'San pedro', 'img' => '29.png'),
            array('id' => '31', 'nro' => '30', 'sig' => 'Santa rosa', 'img' => '30.png'),
            array('id' => '32', 'nro' => '31', 'sig' => 'La luz', 'img' => '31.png'),
            array('id' => '33', 'nro' => '32', 'sig' => 'La plata', 'img' => '32.png'),
            array('id' => '34', 'nro' => '33', 'sig' => 'Cristo', 'img' => '33.png'),
            array('id' => '35', 'nro' => '34', 'sig' => 'La cabeza', 'img' => '34.png'),
            array('id' => '36', 'nro' => '35', 'sig' => 'El pajarito', 'img' => '35.png'),
            array('id' => '37', 'nro' => '36', 'sig' => 'La manteca', 'img' => '36.png'),
            array('id' => '38', 'nro' => '37', 'sig' => 'Dentista', 'img' => '37.png'),
            array('id' => '39', 'nro' => '38', 'sig' => 'Las piedras', 'img' => '38.png'),
            array('id' => '40', 'nro' => '39', 'sig' => 'La lluvia', 'img' => '39.png'),
            array('id' => '41', 'nro' => '40', 'sig' => 'El cura', 'img' => '40.png'),
            array('id' => '42', 'nro' => '41', 'sig' => 'El cuchillo', 'img' => '41.png'),
            array('id' => '43', 'nro' => '42', 'sig' => 'La zapatilla', 'img' => '42.png'),
            array('id' => '44', 'nro' => '43', 'sig' => 'El balcon', 'img' => '43.png'),
            array('id' => '45', 'nro' => '44', 'sig' => 'La carcel', 'img' => '44.png'),
            array('id' => '46', 'nro' => '45', 'sig' => 'El vino', 'img' => '45.png'),
            array('id' => '47', 'nro' => '46', 'sig' => 'Los tomates', 'img' => '46.png'),
            array('id' => '48', 'nro' => '47', 'sig' => 'El muerto', 'img' => '47.png'),
            array('id' => '49', 'nro' => '48', 'sig' => 'Muerto que habla', 'img' => '48.png'),
            array('id' => '50', 'nro' => '49', 'sig' => 'La carne', 'img' => '49.png'),
            array('id' => '51', 'nro' => '50', 'sig' => 'El pan', 'img' => '50.png'),
            array('id' => '52', 'nro' => '51', 'sig' => 'Serrucho', 'img' => '51.png'),
            array('id' => '53', 'nro' => '52', 'sig' => 'Madre e hijo', 'img' => '52.png'),
            array('id' => '54', 'nro' => '53', 'sig' => 'El barco', 'img' => '53.png'),
            array('id' => '55', 'nro' => '54', 'sig' => 'La vaca', 'img' => '54.png'),
            array('id' => '56', 'nro' => '55', 'sig' => 'La música', 'img' => '55.png'),
            array('id' => '57', 'nro' => '56', 'sig' => 'La caida', 'img' => '56.png'),
            array('id' => '58', 'nro' => '57', 'sig' => 'El jorobado', 'img' => '57.png'),
            array('id' => '59', 'nro' => '58', 'sig' => 'El abogado', 'img' => '58.png'),
            array('id' => '60', 'nro' => '59', 'sig' => 'El boludo', 'img' => '59.png'),
            array('id' => '61', 'nro' => '60', 'sig' => 'La virgen', 'img' => '60.png'),
            array('id' => '62', 'nro' => '61', 'sig' => 'La escopeta', 'img' => '61.png'),
            array('id' => '63', 'nro' => '62', 'sig' => 'La inundación', 'img' => '62.png'),
            array('id' => '64', 'nro' => '63', 'sig' => 'El casamiento', 'img' => '63.png'),
            array('id' => '65', 'nro' => '64', 'sig' => 'El llanto', 'img' => '64.png'),
            array('id' => '66', 'nro' => '65', 'sig' => 'Cazador', 'img' => '65.png'),
            array('id' => '67', 'nro' => '66', 'sig' => 'Las lombrices', 'img' => '66.png'),
            array('id' => '68', 'nro' => '67', 'sig' => 'La vibora', 'img' => '67.png'),
            array('id' => '69', 'nro' => '68', 'sig' => 'Los sobrinos', 'img' => '68.png'),
            array('id' => '70', 'nro' => '69', 'sig' => 'Los vicios', 'img' => '69.png'),
            array('id' => '71', 'nro' => '70', 'sig' => 'Muerto en sueño', 'img' => '70.png'),
            array('id' => '72', 'nro' => '71', 'sig' => 'La MeRDa', 'img' => '71.png'),
            array('id' => '73', 'nro' => '72', 'sig' => 'La sorpresa', 'img' => '72.png'),
            array('id' => '74', 'nro' => '73', 'sig' => 'El hospital', 'img' => '73.png'),
            array('id' => '75', 'nro' => '74', 'sig' => 'Gente negra', 'img' => '74.png'),
            array('id' => '76', 'nro' => '75', 'sig' => 'Los besos', 'img' => '75.png'),
            array('id' => '77', 'nro' => '76', 'sig' => 'Llamas', 'img' => '76.png'),
            array('id' => '78', 'nro' => '77', 'sig' => 'Las piernas', 'img' => '77.png'),
            array('id' => '79', 'nro' => '78', 'sig' => 'La prostituta', 'img' => '78.png'),
            array('id' => '80', 'nro' => '79', 'sig' => 'Los ladrones', 'img' => '79.png'),
            array('id' => '81', 'nro' => '80', 'sig' => 'La bocha', 'img' => '80.png'),
            array('id' => '82', 'nro' => '81', 'sig' => 'Las flores', 'img' => '81.png'),
            array('id' => '83', 'nro' => '82', 'sig' => 'La pelea', 'img' => '82.png'),
            array('id' => '84', 'nro' => '83', 'sig' => 'Mal tiempo', 'img' => '83.png'),
            array('id' => '85', 'nro' => '84', 'sig' => 'La iglesia', 'img' => '84.png'),
            array('id' => '86', 'nro' => '85', 'sig' => 'La linterna', 'img' => '85.png'),
            array('id' => '87', 'nro' => '86', 'sig' => 'El humo', 'img' => '86.png'),
            array('id' => '88', 'nro' => '87', 'sig' => 'Los piojos', 'img' => '87.png'),
            array('id' => '89', 'nro' => '88', 'sig' => 'Las papas', 'img' => '88.png'),
            array('id' => '90', 'nro' => '89', 'sig' => 'La rata', 'img' => '89.png'),
            array('id' => '91', 'nro' => '90', 'sig' => 'El miedo', 'img' => '90.png'),
            array('id' => '92', 'nro' => '91', 'sig' => 'Excusado', 'img' => '91.png'),
            array('id' => '93', 'nro' => '92', 'sig' => 'El medico', 'img' => '92.png'),
            array('id' => '94', 'nro' => '93', 'sig' => 'El enamorado', 'img' => '93.png'),
            array('id' => '95', 'nro' => '94', 'sig' => 'El cementerio', 'img' => '94.png'),
            array('id' => '96', 'nro' => '95', 'sig' => 'Los anteojos', 'img' => '95.png'),
            array('id' => '97', 'nro' => '96', 'sig' => 'Marido', 'img' => '96.png'),
            array('id' => '98', 'nro' => '97', 'sig' => 'Mesa', 'img' => '97.png'),
            array('id' => '99', 'nro' => '98', 'sig' => 'Abuelos', 'img' => '98.png'),
            array('id' => '100', 'nro' => '99', 'sig' => 'Los hermanos', 'img' => '99.png')
        );
        $mm = Meaning::all();
        foreach ($mm as $m) {
            $m->delete();
        }
        foreach ($meanings as $meaning) {
            $mean = new Meaning();
            $mean->number = $meaning['nro'];
            $mean->meaning = $meaning['sig'];
            $mean->image = $meaning['img'];
            $mean->save();
        }
    }
}
