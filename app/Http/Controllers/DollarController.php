<?php

namespace App\Http\Controllers;

use App\Dollar;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class DollarController extends Controller
{
    protected $url_dollar = "http://www.ambito.com/economia/mercados/monedas/dolar/";

    public function getDollarExchange()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $content = $goutteClient->request('GET', $this->url_dollar);
        $buy = "$" . $content->filter('div.ultimo>big')->text();
        $sell = "$" . $content->filter('div.cierreAnterior>big')->text();
        $all = Dollar::all();
        foreach ($all as $al) {
            $al->delete();
        }
        $dollar = new Dollar();
        $dollar->buy = $buy;
        $dollar->sell = $sell;
        $dollar->save();
    }
}
