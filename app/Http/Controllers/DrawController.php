<?php

namespace App\Http\Controllers;

use App\Brinco;
use App\Draw;
use App\Loto;
use App\Matutina;
use App\MatutinaDetail;
use App\Meaning;
use App\Nocturna;
use App\NocturnaDetail;
use App\Poceada;
use App\PoceadaPlus;
use App\Primera;
use App\PrimeraDetail;
use App\Quini6;
use App\Vespertina;
use App\VespertinaDetail;
use Carbon\Carbon;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\DomCrawler\Crawler;

class DrawController extends Controller
{
    protected $url_draws = 'http://www.vivitusuerte.com/datospizarra.php?fecha=%s';
    protected $url_detail_draws = 'http://www.vivitusuerte.com/datospizarra_loteria.php?fecha=%s&loteria=%s';
    protected $url_kini6 = 'http://www.vivitusuerte.com/juegos/quini6.php';
    protected $url_loto = 'http://www.vivitusuerte.com/juegos/loto.php';
    protected $url_poceada = 'http://www.vivitusuerte.com/juegos/poceada.php';
    protected $url_poceada_plus = 'http://www.vivitusuerte.com/juegos/quinielaplus.php';
    protected $url_brinco = 'http://www.jugandoonline.com.ar/Brinco/sorteos.asp';
    protected $content_draw = "";

    //protected $current_draw;

    public function __construct()
    {
        //$this->middleware('auth');
        $this->content_draw = "";
    }

    public function getDraw($type, $today)
    {
        if (empty($this->content_draw)) {
            $this->content_draw = self::getContentDraws($today);
        }
        $content = $this->content_draw;
        $results = $content->filter('table > tbody > tr')->each(function ($node) {
            return $node;
        });
        $today_formatted = Carbon::createFromFormat("Y/m/d", $today);
        $today_formatted->setTime(0, 0, 0);
        $draws = Draw::where('date', $today_formatted)->get();
        $counter_draw = $draws->count();
        if ($counter_draw == 0) {
            $draw = new Draw();
            $draw->date = $today_formatted;
            $draw->notified_primera = 0;
            $draw->notified_matutina = 0;
            $draw->notified_vespertina = 0;
            $draw->notified_nocturna = 0;
            $draw->save();
        } else {
            $draw = $draws->first();
        }
        $array_ids_modified = array();
        foreach ($results as $result) {
            $city = $result->filter('td')->first()->text();
            $id_loteria = $result->filter('td')->eq(6)->filter('input[name="id[]"]')->extract(array('value'))[0];
            switch ($type) {
                case 'primera':
                    $value = self::shortNumber($result->filter('td > div')->eq(1)->text());
                    $object = Primera::where('id_draw', $draw->_id)->where('id_loteria', $id_loteria)->first();
                    break;
                case 'matutina':
                    $value = self::shortNumber($result->filter('td > div')->eq(2)->text());
                    $object = Matutina::where('id_draw', $draw->_id)->where('id_loteria', $id_loteria)->first();
                    break;
                case 'vespertina':
                    $value = self::shortNumber($result->filter('td > div')->eq(3)->text());
                    $object = Vespertina::where('id_draw', $draw->_id)->where('id_loteria', $id_loteria)->first();
                    break;
                case 'nocturna':
                    $value = self::shortNumber($result->filter('td > div')->eq(4)->text());
                    $object = Nocturna::where('id_draw', $draw->_id)->where('id_loteria', $id_loteria)->first();
                    break;
            }
            if (isset($object)) {
                $number = substr($value, -2);
                $meaning = Meaning::where('number', $number)->first();
                $object->city = $city;
                if (!empty(trim($value))) {
                    $object->value = $value;
                }
                $object->id_loteria = $id_loteria;
                $object->id_draw = $draw->_id;
                $object->date = $today_formatted;
                if ($number == '--') {
                    $object->meaning = '';
                    $object->meaning_number = '';
                    $object->meaning_image = '';
                } else {
                    $object->meaning = $meaning->meaning;
                    $object->meaning_number = $meaning->number;
                    $object->meaning_image = $meaning->image;
                }
                $object->save();
                self::getDetails($type, $object, $today);
            }
            $array_ids_modified[] = $object->_id;
        }
        \GeneralHelper::instance()->sendIdsToSubscription(implode(',', $array_ids_modified), $type);
    }

    public function getDraws()
    {
        $today = Carbon::now('America/Argentina/Buenos_Aires');
        $today->setTime(0, 0, 0, 0);
        //$today->subDays(1);

        $today = Carbon::createFromFormat('Y-m-d', $today->toDateString());
        $today->setTime(0, 0, 0, 0);
        //self::generateSchema($today);

        $today = $today->format('Y/m/d');
        $this->content_draw = self::getContentDraws($today);
        self::getDraw('primera', $today);
        self::getDraw('matutina', $today);
        self::getDraw('vespertina', $today);
        self::getDraw('nocturna', $today);
    }

    public function getPrimera()
    {
        $today = Carbon::now('America/Argentina/Buenos_Aires');
        $today->setTime(0, 0, 0);
        $today = $today->format('Y/m/d');
        $this->content_draw = self::getContentDraws($today);
        self::getDraw('primera', $today);
    }

    public function getMatutina()
    {
        $today = Carbon::now('America/Argentina/Buenos_Aires');
        $today->setTime(0, 0, 0);
        $today = $today->format('Y/m/d');
        $this->content_draw = self::getContentDraws($today);
        self::getDraw('matutina', $today);
    }

    public function getVespertina()
    {
        $today = Carbon::now('America/Argentina/Buenos_Aires');
        $today->setTime(0, 0, 0);
        $today = $today->format('Y/m/d');
        $this->content_draw = self::getContentDraws($today);
        self::getDraw('vespertina', $today);
    }

    public function getNocturna()
    {
        $today = Carbon::now('America/Argentina/Buenos_Aires');
        $today->setTime(0, 0, 0);
        $today = $today->format('Y/m/d');
        $this->content_draw = self::getContentDraws($today);
        self::getDraw('nocturna', $today);
    }

    private function getDetails($type, $object, $today)
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $main_url = sprintf($this->url_detail_draws, $today, $object->id_loteria);
        $content = $goutteClient->request('GET', $main_url);
        $content->filter('table[align="center"]')->each(function ($node) use ($type, $object) {
            try {
                $title = $node->filter('div[align="center"]')->text();
                $regexp = '/' . substr($type, 0, -1) . '/si';
                preg_match($regexp, $title, $matches);
                if (count($matches) > 0) {
                    $node->filter('tr.texto_cabezas')->each(function ($node) use ($type, $object) {
                        $number_item = str_replace('.', '', $node->filter('td.link_blanco')->eq(0)->text());
                        $value_item = $node->filter('td>div')->eq(0)->text();
                        $oposite_number_item = str_replace('.', '', $node->filter('td.link_blanco')->eq(1)->text());
                        $oposite_value_item = $node->filter('td>div')->eq(1)->text();

                        switch ($type) {
                            case 'primera':
                                if (!empty($number_item) && !empty($value_item)) {
                                    $number = intval($number_item);
                                    if ($number > 0 && $number <= 20) {
                                        $index = $number - 1;
                                        try {
                                            $element = $object->numbers()->where('number', $number);
                                            $element[$index]->value = $value_item;
                                            $element[$index]->save();
                                        } catch (\Exception $e) {
                                            $primera_detail = new PrimeraDetail();
                                            $primera_detail->number = $number;
                                            $primera_detail->value = $value_item;
                                            $object->numbers()->save($primera_detail);
                                        }
                                        //$array_ids_changed[] = $object->_id;
                                        //print_r($array_ids_changed);
                                    }
                                }
                                if (!empty($oposite_number_item) && !empty($oposite_value_item)) {
                                    $number = intval($oposite_number_item);
                                    if ($number > 0 && $number <= 20) {
                                        $index = $number - 1;
                                        try {
                                            $element = $object->numbers()->where('number', $number);
                                            $element[$index]->value = $oposite_value_item;
                                            $element[$index]->save();
                                        } catch (\Exception $e) {
                                            $primera_detail = new PrimeraDetail();
                                            $primera_detail->number = $number;
                                            $primera_detail->value = $oposite_value_item;
                                            $object->numbers()->save($primera_detail);
                                        }
                                        //$array_ids_changed[] = $object->_id;
                                        //print_r($array_ids_changed);
                                    }
                                }
                                break;
                            case 'matutina':
                                if (!empty($number_item) && !empty($value_item)) {
                                    $number = intval($number_item);
                                    if ($number > 0 && $number <= 20) {
                                        $index = $number - 1;
                                        try {
                                            $element = $object->numbers()->where('number', $number);
                                            $element[$index]->value = $value_item;
                                            $element[$index]->save();
                                        } catch (\Exception $e) {
                                            $matutina_detail = new MatutinaDetail();
                                            $matutina_detail->number = $number;
                                            $matutina_detail->value = $value_item;
                                            $object->numbers()->save($matutina_detail);
                                        }
                                    }
                                }

                                if (!empty($oposite_number_item) && !empty($oposite_value_item)) {
                                    $number = intval($oposite_number_item);
                                    if ($number > 0 && $number <= 20) {
                                        $index = $number - 1;
                                        try {
                                            $element = $object->numbers()->where('number', $number);
                                            $element[$index]->value = $oposite_value_item;
                                            $element[$index]->save();
                                        } catch (\Exception $e) {
                                            $matutina_detail = new MatutinaDetail();
                                            $matutina_detail->number = $number;
                                            $matutina_detail->value = $oposite_value_item;
                                            $object->numbers()->save($matutina_detail);
                                        }
                                    }
                                }
                                break;
                            case 'vespertina':
                                if (!empty($number_item) && !empty($value_item)) {
                                    $number = intval($number_item);
                                    if ($number > 0 && $number <= 20) {
                                        $index = $number - 1;
                                        try {
                                            $element = $object->numbers()->where('number', $number);
                                            $element[$index]->value = $value_item;
                                            $element[$index]->save();
                                        } catch (\Exception $e) {
                                            $vespertina_detail = new VespertinaDetail();
                                            $vespertina_detail->number = $number;
                                            $vespertina_detail->value = $value_item;
                                            $object->numbers()->save($vespertina_detail);
                                        }
                                    }
                                }

                                if (!empty($oposite_number_item) && !empty($oposite_value_item)) {
                                    $number = intval($oposite_number_item);
                                    if ($number > 0 && $number <= 20) {
                                        $index = $number - 1;
                                        try {
                                            $element = $object->numbers()->where('number', $number);
                                            $element[$index]->value = $oposite_value_item;
                                            $element[$index]->save();
                                        } catch (\Exception $e) {
                                            $vespertina_detail = new VespertinaDetail();
                                            $vespertina_detail->number = $number;
                                            $vespertina_detail->value = $oposite_value_item;
                                            $object->numbers()->save($vespertina_detail);
                                        }
                                    }
                                }
                                break;
                            case 'nocturna':
                                if (!empty($number_item) && !empty($value_item)) {
                                    $number = intval($number_item);
                                    if ($number > 0 && $number <= 20) {
                                        $index = $number - 1;
                                        try {
                                            $element = $object->numbers()->where('number', $number);
                                            $element[$index]->value = $value_item;
                                            $element[$index]->save();
                                        } catch (\Exception $e) {
                                            $nocturna_detail = new NocturnaDetail();
                                            $nocturna_detail->number = $number;
                                            $nocturna_detail->value = $value_item;
                                            $object->numbers()->save($nocturna_detail);
                                        }
                                    }
                                }

                                if (!empty($oposite_number_item) && !empty($oposite_value_item)) {
                                    $number = intval($oposite_number_item);
                                    if ($number > 0 && $number <= 20) {
                                        $index = $number - 1;
                                        try {
                                            $element = $object->numbers()->where('number', $number);
                                            $element[$index]->value = $oposite_value_item;
                                            $element[$index]->save();
                                        } catch (\Exception $e) {
                                            $nocturna_detail = new NocturnaDetail();
                                            $nocturna_detail->number = $number;
                                            $nocturna_detail->value = $oposite_value_item;
                                            $object->numbers()->save($nocturna_detail);
                                        }
                                    }
                                }
                                break;
                        }
                    });
                }
            } catch (\Exception $exception) {
            }
        });
    }

    public function shortNumber($number)
    {
        if (strlen($number) > 4) {
            $number = substr($number, -4);
        }
        return $number;
    }

    private function getContentDraws($date)
    {
        $main_url = sprintf($this->url_draws, $date);
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        return $goutteClient->request('GET', $main_url);
    }


    public function getQuini6()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $response = $goutteClient->request('GET', $this->url_kini6);
        $draw_number = $response->filter('td.texto_titulo>div.texto_titulo_xl')->text();
        list(, $draw_number) = explode(PHP_EOL, $draw_number);
        $draw_number = chop($draw_number);
        $date = $response->filter('td[colspan="4"]>div.texto_titulo_xl')->text();
        list(, $day, $month, $year) = explode(PHP_EOL, $date);
        $day = trim(str_replace('/', '', $day));
        $month = trim(str_replace('/', '', $month));
        $year = trim($year);
        $date = Carbon::createFromDate($year, $month, $day);
        $date->setTime(0, 0, 0);

        $all_data = $response->filter('table[width="400"]>tr>td.texto_titulo_xl')->reduce(function (Crawler $node, $i) {
            $string = htmlentities($node->text(), null, 'utf-8');
            $content = str_replace("&nbsp;", "", $string);
            $content = html_entity_decode($content);
            if (empty($content)) return false;
            return $node->text();
        });
        $tradicional = $all_data->eq(1)->text();
        $tradicional = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $tradicional));
        $tradicional = explode(PHP_EOL, trim($tradicional));

        $traditional_prizes = array();
        $winners = str_replace('.', '', trim($all_data->eq(3)->text()));
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), $all_data->eq(4)->text()));
        $traditional_prizes[] = ['hits' => '6', 'winners' => $winners, 'prize' => $prize];

        $winners = str_replace('.', '', trim($all_data->eq(6)->text()));
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), $all_data->eq(7)->text()));
        $traditional_prizes[] = ['hits' => '5', 'winners' => $winners, 'prize' => $prize];

        $winners = str_replace('.', '', trim($all_data->eq(9)->text()));
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), $all_data->eq(10)->text()));
        $traditional_prizes[] = ['hits' => '4', 'winners' => $winners, 'prize' => $prize];


        $segunda_vuelta = $all_data->eq(11)->text();
        $segunda_vuelta = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $segunda_vuelta));
        $segunda_vuelta = explode(PHP_EOL, trim($segunda_vuelta));

        $segunda_vuelta_prizes = array();
        $winners = str_replace('.', '', trim($all_data->eq(13)->text()));
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), $all_data->eq(14)->text()));
        $segunda_vuelta_prizes[] = ['hits' => '6', 'winners' => $winners, 'prize' => $prize];

        $winners = str_replace('.', '', trim($all_data->eq(16)->text()));
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), $all_data->eq(17)->text()));
        $segunda_vuelta_prizes[] = ['hits' => '5', 'winners' => $winners, 'prize' => $prize];

        $winners = str_replace('.', '', trim($all_data->eq(19)->text()));
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), $all_data->eq(20)->text()));
        $segunda_vuelta_prizes[] = ['hits' => '4', 'winners' => $winners, 'prize' => $prize];


        $revancha = $all_data->eq(22)->text();
        $revancha = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $revancha));
        $revancha = explode(PHP_EOL, trim($revancha));

        $winners = str_replace('.', '', trim($all_data->eq(24)->text()));
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), $all_data->eq(25)->text()));
        $revancha_prizes = ['winners' => $winners, 'prize' => $prize];


        $siempre_sale = $all_data->eq(26)->text();
        $siempre_sale = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $siempre_sale));
        $siempre_sale = explode(PHP_EOL, trim($siempre_sale));

        $winners = str_replace('.', '', trim($all_data->eq(28)->text()));
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), $all_data->eq(29)->text()));
        $siempre_sale_prizes = ['winners' => $winners, 'prize' => $prize];

        $premio_extra = trim(str_replace('Premio:', '', $response->filter('td.texto>table>tr>td.texto_titulo_xl')->text()));
        $premio_extra = trim(str_replace(array('$', '.', ','), array('', '', '.'), $premio_extra));


        $winners = str_replace('.', '', trim($all_data->eq(31)->text()));
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), $all_data->eq(32)->text()));
        $premio_extra_prizes = ['hits' => '6', 'winners' => $winners, 'prize' => $prize];

        $prize_next_move = $response->filter('div.texto_titulo_xxl')->text();
        list(, $prize_next_move) = explode(PHP_EOL, $prize_next_move);
        $prize_next_move = trim(str_replace(array('$', '.', ','), array('', '', '.'), $prize_next_move));

        Quini6::where('date', $date)->delete();
        $quini6 = new Quini6();
        $quini6->date = $date;
        $quini6->draw_number = $draw_number;
        $quini6->tradicional_numbers = $tradicional;
        $quini6->tradicional_prizes = $traditional_prizes;
        $quini6->segunda_vuelta_numbers = $segunda_vuelta;
        $quini6->segunda_vuelta_prizes = $segunda_vuelta_prizes;
        $quini6->revancha_numbers = $revancha;
        $quini6->revancha_prizes = $revancha_prizes;
        $quini6->siempre_sale_numbers = $siempre_sale;
        $quini6->siempre_sale_prizes = $siempre_sale_prizes;
        $quini6->premio_extra = $premio_extra;
        $quini6->premio_extra_prizes = $premio_extra_prizes;
        $quini6->prize_next_move = $prize_next_move;
        $quini6->notified = 0;
        $quini6->save();
    }

    public function getLoto()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $response = $goutteClient->request('GET', $this->url_loto);
        $draw_number = $response->filter('td.texto_titulo>div[align="center"]')->text();
        list(, , $draw_number) = explode(' ', $draw_number);
        $date = $response->filter('td[colspan="4"]>div.texto_form>span.texto_titulo')->text();
        list(, $day, $month, $year) = explode(PHP_EOL, $date);
        $day = trim(str_replace('/', '', $day));
        $month = trim(str_replace('/', '', $month));
        $year = trim($year);
        $date = Carbon::createFromDate($year, $month, $day);
        $date->setTime(0, 0, 0);


        $all_data = $response->filter('table[width="535"]>tr');

        $tradicional = $all_data->eq(4)->text();
        $tradicional = preg_replace('/[ \t]+/', '', preg_replace('/\s*$^\s*/m', "\n", $tradicional));
        $tradicional = explode(PHP_EOL, trim($tradicional));

        $jackpots = $all_data->eq(5)->text();
        $jackpots = preg_replace('/[ \t]+/', '', preg_replace('/\s*$^\s*/m', "\n", $jackpots));
        $jackpots = explode(PHP_EOL, trim($jackpots));
        $jackpot1_tradicional = $jackpots[1];
        $jackpot2_tradicional = $jackpots[4];

        $tradicional_all_prizes = $all_data->eq(6)->text();
        $tradicional_all_prizes = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $tradicional_all_prizes));
        $tradicional_all_prizes = explode(PHP_EOL, trim($tradicional_all_prizes));

        $label = trim($tradicional_all_prizes[0]);
        $winners = trim($tradicional_all_prizes[1]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[2])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[3]);
        $winners = trim($tradicional_all_prizes[4]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[5])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[6]);
        $winners = trim($tradicional_all_prizes[7]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[8])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[9]);
        $winners = trim($tradicional_all_prizes[10]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[11])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[12]);
        $winners = trim($tradicional_all_prizes[13]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[14])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[15]);
        $winners = trim($tradicional_all_prizes[16]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[17])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[18]);
        $winners = trim($tradicional_all_prizes[19]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[20])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[21]);
        $winners = trim($tradicional_all_prizes[22]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[23])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[24]);
        $winners = trim($tradicional_all_prizes[25]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[26])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[27]);
        $winners = trim($tradicional_all_prizes[28]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[29])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[30]);
        $winners = trim($tradicional_all_prizes[31]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[32])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($tradicional_all_prizes[33]);
        $winners = trim($tradicional_all_prizes[34]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($tradicional_all_prizes[35])));
        $tradicional_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];


        $desquite = $all_data->eq(9)->text();
        $desquite = preg_replace('/[ \t]+/', '', preg_replace('/\s*$^\s*/m', "\n", $desquite));
        $desquite = explode(PHP_EOL, trim($desquite));

        $jackpots = $all_data->eq(10)->text();
        $jackpots = preg_replace('/[ \t]+/', '', preg_replace('/\s*$^\s*/m', "\n", $jackpots));
        $jackpots = explode(PHP_EOL, trim($jackpots));
        $jackpot1_desquite = $jackpots[1];
        $jackpot2_desquite = $jackpots[4];


        $desquite_all_prizes = $all_data->eq(11)->text();
        $desquite_all_prizes = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $desquite_all_prizes));
        $desquite_all_prizes = explode(PHP_EOL, trim($desquite_all_prizes));

        $label = trim($desquite_all_prizes[0]);
        $winners = trim($desquite_all_prizes[1]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($desquite_all_prizes[2])));
        $desquite_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];

        $label = trim($desquite_all_prizes[3]);
        $winners = trim($desquite_all_prizes[4]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($desquite_all_prizes[5])));
        $desquite_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];


        if (count($desquite_all_prizes) == 10) { //breaking one line
            $desquite_all_prizes[6] = $desquite_all_prizes[6] . ' ' . $desquite_all_prizes[7];
            unset($desquite_all_prizes[7]);
            $desquite_all_prizes = array_values($desquite_all_prizes);
        }
        $label = trim($desquite_all_prizes[6]);
        $winners = trim($desquite_all_prizes[7]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($desquite_all_prizes[8])));
        $desquite_prizes[] = ['label' => $label, 'winners' => $winners, 'prize' => $prize];


        $sale_sale = $all_data->eq(14)->text();
        $sale_sale = preg_replace('/[ \t]+/', '', preg_replace('/\s*$^\s*/m', "\n", $sale_sale));
        $sale_sale = explode(PHP_EOL, trim($sale_sale));

        $jackpots = $all_data->eq(15)->text();
        $jackpots = preg_replace('/[ \t]+/', '', preg_replace('/\s*$^\s*/m', "\n", $jackpots));
        $jackpots = explode(PHP_EOL, trim($jackpots));
        $jackpot1_sale_sale = $jackpots[1];
        $jackpot2_sale_sale = $jackpots[4];

        $jackpots = $all_data->eq(16)->text();
        $jackpots = preg_replace('/[ \t]+/', '', preg_replace('/\s*$^\s*/m', "\n", $jackpots));
        $jackpots = explode(PHP_EOL, trim($jackpots));
        $jackpot3_sale_sale = $jackpots[1];
        $jackpot4_sale_sale = $jackpots[4];


        $sale_sale_all_prizes = $all_data->eq(17)->text();
        $sale_sale_all_prizes = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $sale_sale_all_prizes));
        $sale_sale_all_prizes = explode(PHP_EOL, trim($sale_sale_all_prizes));


        $winners = trim($sale_sale_all_prizes[1]);
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($sale_sale_all_prizes[2])));
        $sale_sale_prizes = ['winners' => $winners, 'prize' => $prize];

        $sale_sale_duplicador = trim($sale_sale_all_prizes[4]);

        $prize_next_move = $response->filter('td>div[align="center"].texto_titulo_xl>font')->text();
        $prize_next_move = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $prize_next_move));
        list(, $prize_next_move) = explode(PHP_EOL, trim($prize_next_move));
        $prize_next_move = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($prize_next_move)));

        Loto::where('date', $date)->delete();
        $loto = new Loto();
        $loto->date = $date;
        $loto->draw_number = $draw_number;
        $loto->tradicional_numbers = $tradicional;
        $loto->jackpot1_tradicional = $jackpot1_tradicional;
        $loto->jackpot2_tradicional = $jackpot2_tradicional;
        $loto->tradicional_prizes = $tradicional_prizes;
        $loto->desquite_numbers = $desquite;
        $loto->jackpot1_desquite = $jackpot1_desquite;
        $loto->jackpot2_desquite = $jackpot2_desquite;
        $loto->desquite_prizes = $desquite_prizes;
        $loto->sale_sale_numbers = $sale_sale;
        $loto->jackpot1_sale_sale = $jackpot1_sale_sale;
        $loto->jackpot2_sale_sale = $jackpot2_sale_sale;
        $loto->jackpot3_sale_sale = $jackpot3_sale_sale;
        $loto->jackpot4_sale_sale = $jackpot4_sale_sale;
        $loto->sale_sale_prizes = $sale_sale_prizes;
        $loto->sale_sale_duplicador = $sale_sale_duplicador;
        $loto->prize_next_move = $prize_next_move;
        $loto->notified = 0;
        $loto->save();
    }

    public function getPoceada()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $response = $goutteClient->request('GET', $this->url_poceada);
        $draw_number = $response->filter('td.texto_titulo_xl')->text();
        $draw_number = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $draw_number));
        list(, $draw_number) = explode(PHP_EOL, trim($draw_number));
        list(, $draw_number) = explode(' ', $draw_number);

        $date = $response->filter('td[colspan="4"]>div.texto_form')->text();
        $date = explode(PHP_EOL, $date);
        list($day, $month, $year) = explode('/', trim($date[2]));
        $date = Carbon::createFromDate($year, $month, $day);
        $date->setTime(0, 0, 0);

        $all_data = $response->filter('td.texto_titulo_xl');

        /* optional draw number */
        /*$draw_number = $all_data->eq(0)->text();
        $draw_number = explode(' ', trim($draw_number);
        $draw_number = $draw_number[1];*/

        $poceada_numbers = array();
        for ($i = 1; $i <= 20; $i++) {
            $poceada_numbers[] = trim($all_data->eq($i + 1)->text());
        }

        $winners = trim($all_data->eq(26)->text());
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($all_data->eq(27)->text())));
        $poceada_prizes[] = ['hits' => '8', 'winners' => $winners, 'prize' => $prize];

        $winners = trim($all_data->eq(29)->text());
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($all_data->eq(30)->text())));
        $poceada_prizes[] = ['hits' => '7', 'winners' => $winners, 'prize' => $prize];

        $winners = trim($all_data->eq(32)->text());
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($all_data->eq(33)->text())));
        $poceada_prizes[] = ['hits' => '6', 'winners' => $winners, 'prize' => $prize];

        Poceada::where('date', $date)->delete();
        $poceada = new Poceada();
        $poceada->date = $date;
        $poceada->draw_number = $draw_number;
        $poceada->numbers = $poceada_numbers;
        $poceada->prizes = $poceada_prizes;
        $poceada->notified = 0;
        $poceada->save();
    }

    public function getPoceadaPlus()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $response = $goutteClient->request('GET', $this->url_poceada_plus);

        $all_data_numbers = $response->filter('td.texto_titulo_xxl');
        $poceada_plus_numbers = array();
        for ($i = 1; $i <= 20; $i++) {
            $poceada_plus_numbers[] = trim($all_data_numbers->eq($i - 1)->text());
        }
        $all_data_prizes = $response->filter('table[width="400"]>tr>td.texto_titulo_xl');


        $draw_number = $response->filter('td.Estilo4>div')->text();
        list(, , $draw_number) = explode(' ', trim($draw_number));

        $date = $response->filter('td.texto_titulo_xl>div')->text();
        $date = explode(PHP_EOL, $date);
        list($day, $month, $year) = explode('/', trim($date[2]));
        $date = Carbon::createFromDate(trim($year), trim($month), trim($day));
        $date->setTime(0, 0, 0);


        $winners = trim($all_data_prizes->eq(4)->text());
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($all_data_prizes->eq(5)->text())));
        $poceada_plus_prizes[] = ['hits' => '8', 'winners' => $winners, 'prize' => $prize];

        $winners = trim($all_data_prizes->eq(7)->text());
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($all_data_prizes->eq(8)->text())));
        $poceada_plus_prizes[] = ['hits' => '7', 'winners' => $winners, 'prize' => $prize];

        $winners = trim($all_data_prizes->eq(10)->text());
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($all_data_prizes->eq(11)->text())));
        $poceada_plus_prizes[] = ['hits' => '6', 'winners' => $winners, 'prize' => $prize];

        $winners = trim($all_data_prizes->eq(13)->text());
        if ($winners == 'Vacante') $winners = "0";
        $prize = trim(str_replace(array('$', '.', ','), array('', '', '.'), trim($all_data_prizes->eq(14)->text())));
        $poceada_plus_prizes[] = ['hits' => '5', 'winners' => $winners, 'prize' => $prize];

        PoceadaPlus::where('date', $date)->delete();
        $poceada_plus = new PoceadaPlus();
        $poceada_plus->draw_number = $draw_number;
        $poceada_plus->date = $date;
        $poceada_plus->numbers = $poceada_plus_numbers;
        $poceada_plus->prizes = $poceada_plus_prizes;
        $poceada_plus->notified = 0;
        $poceada_plus->save();
    }

    public function getBrinco()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $response = $goutteClient->request('GET', $this->url_brinco);
        $all_data = $response->filter('table[width="100%"]');
        $all_data = $all_data->eq(4)->filter('tr');
        $draw_number = $all_data->eq(0)->filter('font')->eq(1)->text();
        $date = $all_data->eq(0)->filter('font')->last()->text();
        $date = Carbon::createFromFormat('d/m/Y', $date);
        $date->setTime(0, 0, 0);
        $numbers = $all_data->eq(1)->filter('table>tr>td[height="40"]');
        $array_numbers = array();
        foreach ($numbers as $number) {
            $array_numbers[] = trim($number->textContent);
        }
        $array_prizes = $all_data->eq(4)->filter('table>tr[bgcolor="#F7BBBC"]')->each(function (Crawler $node, $i) {
            if ($i > 0 && $i < 5) {
                $hits = $node->filter('td')->eq(0)->text();
                $winners = $node->filter('td')->eq(1)->text();
                $prize = $node->filter('td')->eq(2)->text();
                $new_row = array();
                $hits = preg_replace('/\s+/S', " ", $hits);
                $regexp = '/[0-9]+/';
                preg_match($regexp, $hits, $hits);
                $hits = $hits[0];
                $winners = intval(str_replace(array(',', '.'), array('', ''), $winners));
                $prize = str_replace(array(',', '.'), array('', ''), $prize);
                $new_row[] = ['hits' => trim($hits), 'winners' => trim($winners), 'prize' => trim($prize)];
                return $new_row;
            }
        });
        $new_array_prizes = array();
        foreach ($array_prizes as $prize) {
            if (!empty($prize)) {
                $new_array_prizes[] = $prize[0];
            }
        }
        $next_prize = $all_data->eq(13)->filter('font[size="4"]')->text();
        $next_prize = str_replace(array('$', ',', '.'), '', $next_prize);
        $next_prize = preg_replace('/\s+/S', " ", $next_prize);
        $next_prize = trim($next_prize);

        Brinco::where('date', $date)->delete();
        $brinco = new Brinco();
        $brinco->draw_number = $draw_number;
        $brinco->date = $date;
        $brinco->numbers = $array_numbers;
        $brinco->prizes = $new_array_prizes;
        $brinco->next_prize = $next_prize;
        $brinco->notified = 0;
        $brinco->save();
    }


}
