<?php

namespace App\Http\Controllers;

use App\Recomendation;
use Illuminate\Http\Request;

class RecomendationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recomendations = Recomendation::all();
        return view('recomendations.index', compact('recomendations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recomendation $recomendation
     * @return \Illuminate\Http\Response
     */
    public function show(Recomendation $recomendation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recomendation $recomendation
     * @return \Illuminate\Http\Response
     */
    public function edit(Recomendation $recomendation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Recomendation $recomendation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recomendation $recomendation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recomendation $recomendation
     * @return \Illuminate\Http\Response
     */
    public function destroy($recomendation_id)
    {
        Recomendation::find($recomendation_id)->delete();
        return redirect()->route('recomendation.index')->with('message', 'El registro se eliminó correctamente');
    }

    public function destroyAll()
    {
        $recomendations = Recomendation::all();
        foreach ($recomendations as $recomendation) {
            $recomendation->delete();
        }
        return redirect()->route('recomendation.index')->with('message', 'Los registros se eliminaron correctamente');
    }
}
