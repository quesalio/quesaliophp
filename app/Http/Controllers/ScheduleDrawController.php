<?php

namespace App\Http\Controllers;

use App\ScheduleDraw;
use Illuminate\Http\Request;

class ScheduleDrawController extends Controller
{
    public function create()
    {
        $days = array();
        $days[] = array('number' => '2', 'day' => 'Lunes');
        $days[] = array('number' => '3', 'day' => 'Martes');
        $days[] = array('number' => '4', 'day' => 'Miércoles');
        $days[] = array('number' => '5', 'day' => 'Jueves');
        $days[] = array('number' => '6', 'day' => 'Viernes');
        $days[] = array('number' => '7', 'day' => 'Sábado');
        $days[] = array('number' => '8', 'day' => 'Domingo');

        $draws = array();
        $draws[] = array('key' => 'brinco', 'name' => 'Brinco');
        $draws[] = array('key' => 'poceada', 'name' => 'Poceada');
        $draws[] = array('key' => 'poceada_plus', 'name' => 'Poceada Plus');
        $draws[] = array('key' => 'quini', 'name' => 'Quini 6');
        $draws[] = array('key' => 'loto', 'name' => 'Loto');
        return view('schedules.create', compact('days', 'draws'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'draw' => 'required',
            'day' => 'required',
            'hour' => 'required'
        ]);
        $schedule = new ScheduleDraw();
        $schedule->draw = $request->post('draw');
        $schedule->day = $request->post('day');
        $schedule->hour = $request->post('hour');
        $schedule->save();
        return redirect()->route('schedule.create')->with('message', 'El registro se guardó correctamente');
    }

    public function index()
    {
        $schedules = ScheduleDraw::all()->sortBy('draw');
        return view('schedules.index', compact('schedules'));
    }

    public function destroy($schedule_id)
    {
        ScheduleDraw::find($schedule_id)->delete();
        return redirect()->route('schedule.index')->with('message', 'El registro se eliminó correctamente');
    }

    public function destroyAll()
    {
        $schedules = ScheduleDraw::all();
        foreach ($schedules as $schedule) {
            $schedule->delete();
        }
        return redirect()->route('schedule.index')->with('message', 'Los registros se eliminaron correctamente');
    }

}
