<?php

namespace App\Http\Controllers;

use App\WeeklyData;
use Illuminate\Http\Request;

class WeeklyDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $weekly = WeeklyData::all();
        if ($weekly->count() > 0) {
            $weekly = $weekly->first();
        }
        return view('weekly.create', compact('weekly'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'number1' => 'required',
            'number2' => 'required',
            'number3' => 'required',
            'number4' => 'required',
            'number5' => 'required',
            'number6' => 'required',
            'date' => 'required'
        ]);
        $weeklies = WeeklyData::all();
        foreach ($weeklies as $weekly) {
            $weekly->delete();
        }
        $weekly = new WeeklyData();
        $weekly->number1 = $request->post('number1');
        $weekly->number2 = $request->post('number2');
        $weekly->number3 = $request->post('number3');
        $weekly->number4 = $request->post('number4');
        $weekly->number5 = $request->post('number5');
        $weekly->number6 = $request->post('number6');
        $weekly->date = $request->post('date');
        $weekly->save();
        return redirect()->route('weekly.create')->with('message', 'El registro se actualizó correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeeklyData $weeklyData
     * @return \Illuminate\Http\Response
     */
    public function show(WeeklyData $weeklyData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeeklyData $weeklyData
     * @return \Illuminate\Http\Response
     */
    public function edit(WeeklyData $weeklyData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\WeeklyData $weeklyData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeeklyData $weeklyData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeeklyData $weeklyData
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeeklyData $weeklyData)
    {
        //
    }
}
