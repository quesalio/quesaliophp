<?php

namespace App\Http\Controllers;

use App\City;
use App\CityId;
use App\MeaningType;
use App\MeaningTypeDetail;
use App\Name;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class UtilsController extends Controller
{
    function buildCities()
    {
        $array_cities = array();
        $array_cities[] = "Buenos Aires";
        $array_cities[] = "Entre Ríos";
        $array_cities[] = "Mendoza";
        $array_cities[] = "Córdoba";
        $array_cities[] = "Corrientes";
        $array_cities[] = "Chaco";
        $array_cities[] = "Santiago";
        $array_cities[] = "Neuquén";
        $array_cities[] = "San Luis";
        $array_cities[] = "Salta";
        $array_cities[] = "Jujuy";
        $array_cities[] = "Tucumán";
        $array_cities[] = "Chubut";
        $array_cities[] = "Formosa";
        $array_cities[] = "Misiones";
        $array_cities[] = "Catamarca";
        $array_cities[] = "San Juan";
        $array_cities[] = "La Rioja";
        $cities = City::all();
        foreach ($cities as $city) {
            $city->delete();
        }
        foreach ($array_cities as $city) {
            $c = new City();
            $c->name = $city;
            $c->save();
        }
    }

    function buildCitiesId()
    {

        $citiesIds = CityId::all();
        foreach ($citiesIds as $citiesId) {
            $citiesId->delete();
        }

        $mondays = array();
        $mondays[] = "25,24,38,23";
        $mondays[] = "25,24,39";
        $mondays[] = "25,24,53";
        $mondays[] = "25,24,28";
        $mondays[] = "25,24,42";
        $mondays[] = "25,24,52";
        $mondays[] = "25,24,48";
        $mondays[] = "25,24,41";
        $mondays[] = "25,24,49,53,28";
        $mondays[] = "25,24,51,50,55";
        $mondays[] = "25,24,50";
        $mondays[] = "25,24,55";
        $mondays[] = "25,24,56";
        $mondays[] = "25,24,59";
        $mondays[] = "25,24,38,60";
        $mondays[] = "25,24,61";
        $mondays[] = "25,24,62";
        $mondays[] = "25,24,63";

        foreach ($mondays as $index => $monday) {
            $position = $index + 1;
            $ci = new CityId();
            $ci->day = 2;
            $ci->position = $position;
            $ci->ids = $monday;
            $ci->save();
        }

        $tuesdays = array();
        $tuesdays[] = "25,24,38,23";
        $tuesdays[] = "25,24,39,23";
        $tuesdays[] = "25,24,53";
        $tuesdays[] = "25,24,28";
        $tuesdays[] = "25,24,42";
        $tuesdays[] = "25,24,52";
        $tuesdays[] = "25,24,48";
        $tuesdays[] = "25,24,41";
        $tuesdays[] = "25,24,49,53,28";
        $tuesdays[] = "25,24,51,50,55";
        $tuesdays[] = "25,24,50";
        $tuesdays[] = "25,24,55";
        $tuesdays[] = "25,24,56";
        $tuesdays[] = "25,24,59";
        $tuesdays[] = "25,24,38,60";
        $tuesdays[] = "25,24,61";
        $tuesdays[] = "25,24,62";
        $tuesdays[] = "25,24,63";

        foreach ($tuesdays as $index => $tuesday) {
            $position = $index + 1;
            $ci = new CityId();
            $ci->day = 3;
            $ci->position = $position;
            $ci->ids = $tuesday;
            $ci->save();
        }

        $wednesdays = array();
        $wednesdays[] = "25,24,38,23,28";
        $wednesdays[] = "25,24,39";
        $wednesdays[] = "25,24,53";
        $wednesdays[] = "25,24,28";
        $wednesdays[] = "25,24,42";
        $wednesdays[] = "25,24,52";
        $wednesdays[] = "25,24,48";
        $wednesdays[] = "25,24,41";
        $wednesdays[] = "25,24,49,53,28";
        $wednesdays[] = "25,24,51,50,55";
        $wednesdays[] = "25,24,50";
        $wednesdays[] = "25,24,55";
        $wednesdays[] = "25,24,56";
        $wednesdays[] = "25,24,59";
        $wednesdays[] = "25,24,38,60";
        $wednesdays[] = "25,24,61";
        $wednesdays[] = "25,24,62";
        $wednesdays[] = "25,24,63";

        foreach ($wednesdays as $index => $wednesday) {
            $position = $index + 1;
            $ci = new CityId();
            $ci->day = 4;
            $ci->position = $position;
            $ci->ids = $wednesday;
            $ci->save();
        }

        $thursdays = array();
        $thursdays[] = "25,24,38,23";
        $thursdays[] = "25,24,39";
        $thursdays[] = "25,24,53";
        $thursdays[] = "25,24,28";
        $thursdays[] = "25,24,42";
        $thursdays[] = "25,24,52";
        $thursdays[] = "25,24,48";
        $thursdays[] = "25,24,41";
        $thursdays[] = "25,24,49,53,28";
        $thursdays[] = "25,24,51,50,55";
        $thursdays[] = "25,24,50";
        $thursdays[] = "25,24,55";
        $thursdays[] = "25,24,56";
        $thursdays[] = "25,24,59";
        $thursdays[] = "25,24,38,60";
        $thursdays[] = "25,24,61";
        $thursdays[] = "25,24,62";
        $thursdays[] = "25,24,63";

        foreach ($thursdays as $index => $thursday) {
            $position = $index + 1;
            $ci = new CityId();
            $ci->day = 5;
            $ci->position = $position;
            $ci->ids = $thursday;
            $ci->save();
        }

        $fridays = array();
        $fridays[] = "25,24,38,23,48";
        $fridays[] = "25,24,39";
        $fridays[] = "25,24,53";
        $fridays[] = "25,24,28,48";
        $fridays[] = "25,24,42";
        $fridays[] = "25,24,52";
        $fridays[] = "25,24,48";
        $fridays[] = "25,24,41";
        $fridays[] = "25,24,49,53,28";
        $fridays[] = "25,24,51,50,55";
        $fridays[] = "25,24,50";
        $fridays[] = "25,24,55";
        $fridays[] = "25,24,56";
        $fridays[] = "25,24,59";
        $fridays[] = "25,24,38,60";
        $fridays[] = "25,24,61";
        $fridays[] = "25,24,62";
        $fridays[] = "25,24,63";

        foreach ($fridays as $index => $friday) {
            $position = $index + 1;
            $ci = new CityId();
            $ci->day = 6;
            $ci->position = $position;
            $ci->ids = $friday;
            $ci->save();
        }

        $saturdays = array();
        $saturdays[] = "25,24,38,23,53";
        $saturdays[] = "25,24,39,23";
        $saturdays[] = "25,24,53";
        $saturdays[] = "25,24,28";
        $saturdays[] = "25,24,42";
        $saturdays[] = "25,24,52";
        $saturdays[] = "25,24,48";
        $saturdays[] = "25,24,41";
        $saturdays[] = "25,24,49,53,28";
        $saturdays[] = "25,24,51,50,55";
        $saturdays[] = "25,24,50";
        $saturdays[] = "25,24,55";
        $saturdays[] = "25,24,56";
        $saturdays[] = "25,24,59";
        $saturdays[] = "25,24,38,60";
        $saturdays[] = "25,24,61";
        $saturdays[] = "25,24,62";
        $saturdays[] = "25,24,63";

        foreach ($saturdays as $index => $saturday) {
            $position = $index + 1;
            $ci = new CityId();
            $ci->day = 7;
            $ci->position = $position;
            $ci->ids = $saturday;
            $ci->save();
        }

        $sundays = array();
        $sundays[] = "25,24,38,23,53";
        $sundays[] = "25,24,39,23";
        $sundays[] = "25,24,53";
        $sundays[] = "25,24,28";
        $sundays[] = "25,24,42";
        $sundays[] = "25,24,52";
        $sundays[] = "25,24,48";
        $sundays[] = "25,24,41";
        $sundays[] = "25,24,49,53,28";
        $sundays[] = "25,24,51,50,55";
        $sundays[] = "25,24,50";
        $sundays[] = "25,24,55";
        $sundays[] = "25,24,56";
        $sundays[] = "25,24,59";
        $sundays[] = "25,24,38,60";
        $sundays[] = "25,24,61";
        $sundays[] = "25,24,62";
        $sundays[] = "25,24,63";

        foreach ($sundays as $index => $sunday) {
            $position = $index + 1;
            $ci = new CityId();
            $ci->day = 1;
            $ci->position = $position;
            $ci->ids = $sunday;
            $ci->save();
        }
    }

    function buildMeaningTypes()
    {
        $meaningTypes = MeaningType::all();
        foreach ($meaningTypes as $meaningType) {
            $meaningType->delete();
        }
        $array_files = ['dreams.json', 'animals.json', 'careers.json', 'name.json', 'simpatico.json', 'martin_gala.json'];
        $array_meanings = array();
        $array_meanings[] = ["SUEÑOS", 0];
        $array_meanings[] = ["ANIMALES", 0];
        $array_meanings[] = ["OFICIOS", 0];
        $array_meanings[] = ["NOMBRES", 1];
        $array_meanings[] = ["SIMPÁTICOS", 0];
        $array_meanings[] = ["MARTIN GALA", 0];
        foreach ($array_meanings as $index => $meaning) {
            $name = $meaning[0];
            $isNames = $meaning[1];
            $meaningType = new MeaningType();
            $meaningType->name = $name;
            $meaningType->isName = $isNames;
            $meaningType->position = $index + 1;
            $meaningType->save();
            $file = $array_files[$index];
            $json = file_get_contents(public_path("json/" . $file));
            $json_data = json_decode($json, true);
            foreach ($json_data as $item) {
                $image = $item['image'];
                $number = $item['number'];
                $meaning = $item['meaning'];
                //$idMeaningType = $item['idMeaningType'];
                $mt = new MeaningTypeDetail();
                $mt->image = $image;
                $mt->number = $number;
                $mt->meaning = $meaning;
                //$mt->idMeaningType = $idMeaningType;
                $meaningType->details()->save($mt);
            }
        }
    }

    function getNames()
    {
        $names = Name::all();
        foreach ($names as $name) {
            $name->delete();
        }
        $array_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $url_base = 'http://www.loteriasmundiales.com.ar/index.asp?pagina=n';
        foreach ($array_letters as $letter) {
            $content = $this->getContent($url_base . $letter);
            $numbers = $content->filter('td.nro_nom')->each(function ($node) {
                return trim(strip_tags($node->html()));
            });
            $names = $content->filter('td.txt_nom')->each(function ($node) {
                return trim(strip_tags($node->html()));
            });
            foreach ($numbers as $index => $number) {
                $name = new Name();
                $name->number = $number;
                $name->name = $names[$index];
                $name->letter = strtoupper($letter);
                $name->save();
            }
        }
    }

    private function getContent($url)
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        return $goutteClient->request('GET', $url);
    }
}
