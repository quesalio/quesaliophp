<?php

namespace App\Http\Controllers;

use App\Ephemerides;
use Gumlet\ImageResize;
use Gumlet\ImageResizeException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

class EphemeridesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ephemerides = Ephemerides::all();
        return view('ephemerides.index', compact('ephemerides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ephemerides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'number' => 'required',
            'image' => 'required',
            'description' => 'required'
        ]);
        $image_name = "";
        $image = $request->post('image');
        if (!empty($image)) {
            $extension = strtolower(pathinfo($image, PATHINFO_EXTENSION));
            $image_name = 'ephemerides/' . uniqid(rand()) . '.' . $extension;
            $client = new Client(array(
                'timeout' => 60,
            ));
            try {
                $client->request('GET', $image, ['sink' => $image_name]);
                $img = new ImageResize(public_path($image_name));
                $img->resize(360, 200);
                $img->save($image_name);
            } catch (ImageResizeException $e) {
            } catch (GuzzleException $e) {
            }
        }
        $ephemerides = new Ephemerides();
        $ephemerides->number = $request->post('number');
        if (!empty($image_name)) {
            $ephemerides->image = $image_name;
        }
        $ephemerides->description = $request->post('description');
        $ephemerides->save();


        return redirect()->route('ephemerides.create')->with('message', 'El registro se guardó correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ephemerides $ephemerides
     * @return \Illuminate\Http\Response
     */
    public function show(Ephemerides $ephemerides)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ephemerides $ephemerides
     * @return \Illuminate\Http\Response
     */
    public function edit($ephemerides_id)
    {
        $ephemerides = Ephemerides::find($ephemerides_id);
        return view('ephemerides.edit', compact('ephemerides'));
    }

    public function update(Request $request, $ephemerides_id)
    {
        $this->validate($request, [
            'number' => 'required',
            'image' => 'required',
            'description' => 'required'
        ]);
        $image_name = "";
        $image = $request->post('image');
        if (!empty($image)) {
            $extension = strtolower(pathinfo($image, PATHINFO_EXTENSION));
            $image_name = 'ephemerides/' . uniqid(rand()) . '.' . $extension;
            $client = new Client(array(
                'timeout' => 60,
            ));
            try {
                $client->request('GET', $image, ['sink' => $image_name]);
                $img = new ImageResize(public_path($image_name));
                $img->resize(360, 200);
                $img->save($image_name);
            } catch (ImageResizeException $e) {
            } catch (GuzzleException $e) {
            }
        }
        $ephemerides = Ephemerides::find($ephemerides_id);
        $ephemerides->number = $request->post('number');
        if (!empty($image_name)) {
            $ephemerides->image = $image_name;
        }
        $ephemerides->description = $request->post('description');
        $ephemerides->save();
        return redirect()->route('ephemerides.index')->with('message', 'El registro se actualizó correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ephemerides $ephemerides
     * @return \Illuminate\Http\Response
     */
    public function destroy($ephemerides_id)
    {
        Ephemerides::find($ephemerides_id)->delete();
        return redirect()->route('ephemerides.index')->with('message', 'El registro se eliminó correctamente');
    }

    public function destroyAll()
    {
        $ephemerides = Ephemerides::all();
        foreach ($ephemerides as $ephemeride) {
            $ephemeride->delete();
        }
        return redirect()->route('ephemerides.index')->with('message', 'Los registros se eliminaron correctamente');
    }
}
