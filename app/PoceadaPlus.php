<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PoceadaPlus extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "poceada_plus";
    protected $dates = ['date'];
}