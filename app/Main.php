<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Main extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "main";
    protected $dates = ['date'];
}

