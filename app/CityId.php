<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CityId extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "city_id";
}