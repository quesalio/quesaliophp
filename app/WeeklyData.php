<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class WeeklyData extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "weekly_data";
    protected $dates = ['date'];
}
