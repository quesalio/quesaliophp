<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class MeaningType extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "meaning_type";

    public function details()
    {
        return $this->embedsMany(MeaningTypeDetail::class);
    }

}
