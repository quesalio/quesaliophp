<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Brinco extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "brinco";
    protected $dates = ['date'];
}
