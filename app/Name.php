<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Name extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "names";
}
