<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Meaning extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "meanings";

}
