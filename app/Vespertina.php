<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Vespertina extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "vespertina";
    protected $dates = ["date"];
    public function numbers()
    {
        return $this->embedsMany(VespertinaDetail::class);
    }
}
