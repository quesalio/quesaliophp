<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Matutina extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "matutina";
    protected $dates = ["date"];
    public function numbers()
    {
        return $this->embedsMany(MatutinaDetail::class);
    }
}
