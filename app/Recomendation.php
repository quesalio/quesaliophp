<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Recomendation extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "recomendations";
    protected $dates = ['date'];
}