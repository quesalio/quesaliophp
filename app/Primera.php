<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Primera extends Eloquent
{
    protected $connection = "mongodb";
    protected $collection = "primera";
    protected $dates = ["date"];

    public function numbers()
    {
        return $this->embedsMany(PrimeraDetail::class);
    }
}
