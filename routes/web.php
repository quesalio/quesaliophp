<?php

Route::get('/', function () {
    return view('welcome');
});

//TODO falta poner en el panel para hacer el primer fetch
Route::get('crear_ciudades', 'UtilsController@buildCities')->name('build.cities');
Route::get('crear_id_ciudades', 'UtilsController@buildCitiesId')->name('build.cities.id');
Route::get('crear_significados', 'UtilsController@buildMeaningTypes')->name('build.meaning.types');
Route::get('crear_nombres', 'UtilsController@getNames')->name('get.names');


//pruebas para la suscripción después de la ejecución del scrap
///Route::get('suscripcion','DrawController@testSubscription')->name('subscription');

Route::get('iniciar_sorteo/{draw}', 'ConfigurationController@startDraw')->name('start.draw');
Route::get('parar_sorteo', 'ConfigurationController@stopDraw')->name('stop.draw');

Route::get('sorteos', 'DrawController@getDraws')->name('sorteos');
Route::get('primera', 'DrawController@getPrimera')->name('primera');
Route::get('matutina', 'DrawController@getMatutina')->name('matutina');
Route::get('vespertina', 'DrawController@getVespertina')->name('vespertina');
Route::get('nocturna', 'DrawController@getNocturna')->name('nocturna');
Route::get('esquema', 'HomeController@generateSchema')->name('esquema');

Route::get('quini6', 'DrawController@getQuini6')->name('quini6');
Route::get('brinco', 'DrawController@getBrinco')->name('brinco');
Route::get('loto', 'DrawController@getLoto')->name('loto');
Route::get('poceada', 'DrawController@getPoceada')->name('poceada');
Route::get('poceada_plus', 'DrawController@getPoceadaPlus')->name('poceada_plus');
Route::get('dolar', 'DollarController@getDollarExchange')->name('dolar');
Route::get('fecha_actual', 'ConfigurationController@getCurrentDate')->name('current.date');
Route::get('create_meanings', 'MeaningController@createMeanings')->name('create_meanings');

Route::get('check_brinco', 'NotificationController@checkBrinco')->name('check.brinco');
Route::get('check_poceada', 'NotificationController@checkPoceada')->name('check.poceada');
Route::get('check_poceada_plus', 'NotificationController@checkPoceadaPlus')->name('check.poceada.plus');
Route::get('check_loto', 'NotificationController@checkLoto')->name('check.loto');
Route::get('check_quini', 'NotificationController@checkQuini')->name('check.quini');


Route::get('notificacion/{draw}', 'NotificationController@sendNotification')->name('send.notification');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth'])->group(function () {
    Route::prefix('dashboard')->group(function () {
        Route::get('/', 'DashboardController@index')->name('index');
//schedule
        Route::get('calendar/create', 'ScheduleDrawController@create')->name('schedule.create');
        Route::post('calendar', 'ScheduleDrawController@store')->name('schedule.store');
        Route::get('calendar', 'ScheduleDrawController@index')->name('schedule.index');
        Route::delete('calendar/{calendar}', 'ScheduleDrawController@destroy')->name('schedule.destroy');
        Route::delete('calendar', 'ScheduleDrawController@destroyAll')->name('schedule.destroy.all');
        //weekly
        Route::get('weekly/create', 'WeeklyDataController@create')->name('weekly.create');
        Route::post('weekly', 'WeeklyDataController@store')->name('weekly.store');

        //configuration
        Route::get('configuration', 'ConfigurationController@create')->name('configuration.create');
        Route::post('configuration', 'ConfigurationController@store')->name('configuration.store');
        Route::get('configuration/draw/start/{draw}', 'ConfigurationController@startDraw')->name('configuration.start.draw');
        Route::get('configuration/draw/stop', 'ConfigurationController@stopDraw')->name('configuration.stop.draw');

        //ephemerides
        Route::get('ephemerides/create', 'EphemeridesController@create')->name('ephemerides.create');
        Route::get('ephemerides', 'EphemeridesController@index')->name('ephemerides.index');
        Route::post('ephemerides', 'EphemeridesController@store')->name('ephemerides.store');
        Route::get('ephemerides/{ephemeride}', 'EphemeridesController@show')->name('ephemerides.show');
        Route::get('ephemerides/{ephemeride}/edit', 'EphemeridesController@edit')->name('ephemerides.edit');
        Route::patch('ephemerides/{ephemeride}', 'EphemeridesController@update')->name('ephemerides.update');
        Route::delete('ephemerides/{ephemeride}', 'EphemeridesController@destroy')->name('ephemerides.destroy');
        Route::delete('ephemerides', 'EphemeridesController@destroyAll')->name('ephemerides.destroy.all');

        //manual jobs executed from frontend
        Route::get('primera', 'HomeController@primera')->name('manual.primera');
        Route::get('matutina', 'HomeController@matutina')->name('manual.matutina');
        Route::get('vespertina', 'HomeController@vespertina')->name('manual.vespertina');
        Route::get('nocturna', 'HomeController@nocturna')->name('manual.nocturna');
        Route::get('dollar', 'HomeController@dollar')->name('manual.dollar');
        Route::get('schema', 'HomeController@createSchema')->name('manual.create.schema');
        Route::post('schema', 'HomeController@storeSchema')->name('manual.store.schema');
        Route::get('poceada', 'HomeController@poceada')->name('manual.poceada');
        Route::get('poceada_plus', 'HomeController@poceadaPlus')->name('manual.poceada_plus');
        Route::get('quini', 'HomeController@quini')->name('manual.quini');
        Route::get('loto', 'HomeController@loto')->name('manual.loto');
        Route::get('brinco', 'HomeController@brinco')->name('manual.brinco');


        //modify values of draws
        Route::get('primera/edit', 'HomeController@editPrimera')->name('draws.primera');
        Route::post('primera/detail', 'HomeController@detailPrimera')->name('draws.primera.detail');
        Route::post('primera/store', 'HomeController@storeDetailPrimera')->name('draws.store.primera.detail');
        Route::get('primera/{primera}/edit', 'HomeController@editNumbersPrimera')->name('draws.numbers.primera');
        Route::post('primera/edit/store', 'HomeController@storeNumbersPrimera')->name('draws.store.numbers.primera');

        Route::get('matutina/edit', 'HomeController@editMatutina')->name('draws.matutina');
        Route::post('matutina/detail', 'HomeController@detailMatutina')->name('draws.matutina.detail');
        Route::post('matutina/store', 'HomeController@storeDetailMatutina')->name('draws.store.matutina.detail');
        Route::get('matutina/{matutina}/edit', 'HomeController@editNumbersMatutina')->name('draws.numbers.matutina');
        Route::post('matutina/edit/store', 'HomeController@storeNumbersMatutina')->name('draws.store.numbers.matutina');

        Route::get('vespertina/edit', 'HomeController@editVespertina')->name('draws.vespertina');
        Route::post('vespertina/detail', 'HomeController@detailVespertina')->name('draws.vespertina.detail');
        Route::post('vespertina/store', 'HomeController@storeDetailVespertina')->name('draws.store.vespertina.detail');
        Route::get('vespertina/{vespertina}/edit', 'HomeController@editNumbersVespertina')->name('draws.numbers.vespertina');
        Route::post('vespertina/edit/store', 'HomeController@storeNumbersVespertina')->name('draws.store.numbers.vespertina');

        Route::get('nocturna/edit', 'HomeController@editNocturna')->name('draws.nocturna');
        Route::post('nocturna/detail', 'HomeController@detailNocturna')->name('draws.nocturna.detail');
        Route::post('nocturna/store', 'HomeController@storeDetailNocturna')->name('draws.store.nocturna.detail');
        Route::get('nocturna/{nocturna}/edit', 'HomeController@editNumbersNocturna')->name('draws.numbers.nocturna');
        Route::post('nocturna/edit/store', 'HomeController@storeNumbersNocturna')->name('draws.store.numbers.nocturna');

        Route::get('poceada/edit', 'HomeController@editPoceada')->name('draws.poceada');
        Route::post('poceada/detail', 'HomeController@detailPoceada')->name('draws.poceada.detail');
        Route::post('poceada/create', 'HomeController@createDetailPoceada')->name('draws.poceada.detail.create');
        Route::post('poceada/delete', 'HomeController@deleteDetailPoceada')->name('draws.poceada.detail.delete');
        Route::post('poceada/store', 'HomeController@storeDetailPoceada')->name('draws.store.poceada.detail');

        Route::get('poceada_plus/edit', 'HomeController@editPoceadaPlus')->name('draws.poceada.plus');
        Route::post('poceada_plus/detail', 'HomeController@detailPoceadaPlus')->name('draws.poceada.plus.detail');
        Route::post('poceada_plus/create', 'HomeController@createDetailPoceadaPlus')->name('draws.poceada.plus.detail.create');
        Route::post('poceada_plus/delete', 'HomeController@deleteDetailPoceadaPlus')->name('draws.poceada.plus.detail.delete');
        Route::post('poceada_plus/store', 'HomeController@storeDetailPoceadaPlus')->name('draws.store.poceada.plus.detail');

        Route::get('brinco/edit', 'HomeController@editBrinco')->name('draws.brinco');
        Route::post('brinco/detail', 'HomeController@detailBrinco')->name('draws.brinco.detail');
        Route::post('brinco/create', 'HomeController@createDetailBrinco')->name('draws.brinco.detail.create');
        Route::post('brinco/delete', 'HomeController@deleteDetailBrinco')->name('draws.brinco.detail.delete');
        Route::post('brinco/store', 'HomeController@storeDetailBrinco')->name('draws.store.brinco.detail');

        Route::get('quini/edit', 'HomeController@editQuini')->name('draws.quini');
        Route::post('quini/detail', 'HomeController@detailQuini')->name('draws.quini.detail');
        Route::post('quini/create', 'HomeController@createDetailQuini')->name('draws.quini.detail.create');
        Route::post('quini/delete', 'HomeController@deleteDetailQuini')->name('draws.quini.detail.delete');
        Route::post('quini/store', 'HomeController@storeDetailQuini')->name('draws.store.quini.detail');

        Route::get('loto/edit', 'HomeController@editLoto')->name('draws.loto');
        Route::post('loto/detail', 'HomeController@detailLoto')->name('draws.loto.detail');
        Route::post('loto/create', 'HomeController@createDetailLoto')->name('draws.loto.detail.create');
        Route::post('loto/delete', 'HomeController@deleteDetailLoto')->name('draws.loto.detail.delete');
        Route::post('loto/store', 'HomeController@storeDetailLoto')->name('draws.store.loto.detail');

        Route::get('recomendation', 'RecomendationController@index')->name('recomendation.index');
        Route::delete('recomendation/{ephemeride}', 'RecomendationController@destroy')->name('recomendation.destroy');
        Route::delete('recomendation', 'RecomendationController@destroyAll')->name('recomendation.destroy.all');

    });
});

Route::get('/image', 'NotificationController@generateImage');

