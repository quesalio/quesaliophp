/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});


$(document).ready(function () {
    $('#btnShowPrimeraContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_primera_detail,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#primeraContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    $('#btnShowMatutinaContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_matutina_detail,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#matutinaContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    $('#btnShowVespertinaContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_vespertina_detail,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#vespertinaContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    $('#btnShowNocturnaContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_nocturna_detail,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#nocturnaContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    function getPoceadaContent() {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_poceada_detail,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#poceadaContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    $('#btnShowPoceadaContent').on('click', function (e) {
        getPoceadaContent();
    });
    $('#btnCreatePoceadaContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_poceada_detail_create,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                getPoceadaContent();
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    $('#btnDeletePoceadaContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_poceada_detail_delete,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#poceadaContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    function getPoceadaPlusContent() {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_poceada_plus_detail,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#poceadaPlusContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    $('#btnShowPoceadaPlusContent').on('click', function (e) {
        getPoceadaPlusContent();
    });
    $('#btnCreatePoceadaPlusContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_poceada_plus_detail_create,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                getPoceadaPlusContent();
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    $('#btnDeletePoceadaPlusContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_poceada_plus_detail_delete,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#poceadaPlusContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    function getBrincoContent() {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_brinco_detail,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#brincoContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    $('#btnShowBrincoContent').on('click', function (e) {
        getBrincoContent();
    });
    $('#btnCreateBrincoContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_brinco_detail_create,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                getBrincoContent();
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    $('#btnDeleteBrincoContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_brinco_detail_delete,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#brincoContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    function getQuiniContent() {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_quini_detail,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#quiniContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    $('#btnShowQuiniContent').on('click', function (e) {
        getQuiniContent();
    });
    $('#btnCreateQuiniContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_quini_detail_create,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                getQuiniContent();
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    $('#btnDeleteQuiniContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_quini_detail_delete,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#quiniContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    function getLotoContent() {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_loto_detail,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#lotoContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    $('#btnShowLotoContent').on('click', function (e) {
        getLotoContent();
    });
    $('#btnCreateLotoContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_loto_detail_create,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                getLotoContent();
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    $('#btnDeleteLotoContent').on('click', function (e) {
        let date = $('#date').val();
        let data = {date: date};
        $.ajax({
            type: "post",
            url: config.path_loto_detail_delete,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let container = $('#lotoContent');
                container.empty();
                container.append(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
});