@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif

                <div class="form-group">
                    <label for="date">Fecha</label>
                    <input type="date" class="form-control" id="date" name="date"
                           value="@php echo date('Y-m-d'); @endphp"
                           placeholder="Fecha del sorteo">
                </div>
                <button type="button" class="btn btn-primary" id="btnShowLotoContent">Mostrar</button>
                <button type="button" class="btn btn-info" id="btnCreateLotoContent">Create</button>
                <button type="button" class="btn btn-danger" id="btnDeleteLotoContent">Eliminar</button>
                <p></p>
                <div id="lotoContent">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        let config = {
            path_loto_detail: '{{route('draws.loto.detail')}}',
            path_loto_detail_create: '{{route('draws.loto.detail.create')}}',
            path_loto_detail_delete: '{{route('draws.loto.detail.delete')}}',
        }
    </script>
@endsection