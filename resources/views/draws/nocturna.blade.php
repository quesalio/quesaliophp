@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif

                <div class="form-group">
                    <label for="date">Fecha</label>
                    <input type="date" class="form-control" id="date" name="date"
                           value="@php echo date('Y-m-d'); @endphp"
                           placeholder="Fecha del sorteo">
                </div>
                <button type="button" class="btn btn-primary" id="btnShowNocturnaContent">Mostrar</button>
                <p></p>
                <div id="nocturnaContent"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        let config = {path_nocturna_detail: '{{route('draws.nocturna.detail')}}'}
    </script>
@endsection