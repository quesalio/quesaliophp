<div class="container jumbotron">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <form method="post" action="{{route('draws.store.brinco.detail')}}">
                @csrf
                @php
                    $numbers=$brinco->numbers;
                @endphp
                @for($i=0;$i<6;$i++)
                    @php
                        try{
                            $number=$numbers[$i];
                        }catch (Exception $e){
                            $number="";
                        }
                    @endphp
                    <div class="form-group">
                        <label for="numbers[]">Número #{{$i+1}}</label>
                        <input type="text" class="form-control" id="numbers[]" name="numbers[]"
                               value="{{$number}}"/>
                    </div>
                @endfor
                @php
                    $prizes=$brinco->prizes;
                @endphp
                @for($i=3;$i<=6;$i++)
                    @php
                        try{
                            $prize_row=$prizes[$i-3];
                            $hits=$prize_row['hits'];
                            $winners=$prize_row['winners'];
                            $prize=$prize_row['prize'];
                        }catch (Exception $e){
                            $hits=$i;
                            $winners=0;
                            $prize=0;
                        }
                    @endphp
                    <div class="form-group">
                        <input type="hidden" id="hits[]" name="hits[]" value="{{$hits}}"/>
                        <label for="winners[] prizes[]">{{$hits}} Aciertos</label>
                        <input type="text" class="span6" id="winners[]" name="winners[]"
                               value="{{$winners}}"/>Ganadores
                        <input type="text" class="span6" id="prizes[]" name="prizes[]"
                               value="{{$prize}}"/>Premio
                    </div>
                @endfor
                @php
                    try{
                        $next_prize=$brinco->next_prize;
                    }catch (Exception $exception){
                        $next_prize="";
                    }

                @endphp
                <div class="form-group">
                    <label for="next_prize">Próximo Premio</label>
                    <input type="text" class="form-control" id="next_prize" name="next_prize"
                           value="{{$next_prize}}"/>
                </div>
                <input type="hidden" name="id_brinco" value="{{$brinco->_id}}"/>
                <button type="submit" class="btn btn-primary" id="btnSaveBrincoContent">Guardar</button>
            </form>
        </div>
    </div>
</div>