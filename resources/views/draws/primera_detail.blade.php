<div class="container jumbotron">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <form method="post" action="{{route('draws.store.primera.detail')}}">
                @csrf
                @foreach($primeras as $index=>$primera)
                    <div class="form-group">
                        <label for="city_text">{{$primera->city}}</label>
                        <input type="text" class="form-control" id="values[]" name="values[]"
                               value="{{$primera->value}}">
                        <input type="hidden" name="cities[]" value="{{$primera->_id}}"/>
                        <a href="{{route('draws.numbers.primera',['id_primera'=>$primera->_id])}}">
                            Modificar Números
                        </a>
                    </div>
                @endforeach
                <input type="hidden" name="id_draw" value="{{$draw->_id}}"/>
                <button type="submit" class="btn btn-primary" id="btnSavePrimeraContent">Guardar</button>
            </form>
        </div>
    </div>
</div>