<div class="container jumbotron">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <form method="post" action="{{route('draws.store.poceada.plus.detail')}}">
                @csrf
                @php
                    $numbers=$poceada_plus->numbers;
                @endphp
                @for($i=0;$i<20;$i++)
                    @php
                        try{
                            $number=$numbers[$i];
                        }catch (Exception $e){
                            $number="";
                        }
                    @endphp
                    <div class="form-group">
                        <label for="numbers[]">Número #{{$i+1}}</label>
                        <input type="text" class="form-control" id="numbers[]" name="numbers[]"
                               value="{{$number}}"/>
                    </div>
                @endfor
                @php
                    $prizes=$poceada_plus->prizes;
                @endphp
                @for($i=5;$i<=8;$i++)
                    @php

                        try{
                            $prize_row=$prizes[$i-5];
                            $hits=$prize_row['hits'];
                            $winners=$prize_row['winners'];
                            $prize=$prize_row['prize'];
                        }catch (Exception $e){
                            $hits=$i;
                            $winners=0;
                            $prize=0;
                        }
                    @endphp
                    <div class="form-group">
                        <input type="hidden" id="hits[]" name="hits[]" value="{{$hits}}"/>
                        <label for="winners[] prizes[]">{{$hits}} Aciertos</label>
                        <input type="text" class="span6" id="winners[]" name="winners[]"
                               value="{{$winners}}"/>Ganadores
                        <input type="text" class="span6" id="prizes[]" name="prizes[]"
                               value="{{$prize}}"/>Premio
                    </div>
                @endfor
                <input type="hidden" name="id_poceada_plus" value="{{$poceada_plus->_id}}"/>
                <button type="submit" class="btn btn-primary" id="btnSavePoceadaPlusContent">Guardar</button>
            </form>
        </div>
    </div>
</div>