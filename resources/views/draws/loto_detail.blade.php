<div class="container jumbotron">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <form method="post" action="{{route('draws.store.loto.detail')}}">
                @csrf
                <h2>Tradicional Primer Sorteo</h2>
                @php
                    $tradicional_numbers=$loto->tradicional_numbers;
                @endphp
                @for($i=0;$i<6;$i++)
                    @php
                        try{
                            $tradicional_number=$tradicional_numbers[$i];
                        }catch (Exception $e){
                            $tradicional_number="";
                        }
                    @endphp
                    <div class="form-group">
                        <label for="numbers[]">Número #{{$i+1}}</label>
                        <input type="text" class="form-control" id="tradicional_numbers[]" name="tradicional_numbers[]"
                               value="{{$tradicional_number}}"/>
                    </div>
                @endfor
                @php
                    try{
                        $jackpot1_tradicional=$loto->jackpot1_tradicional;
                    }catch (Exception $e){
                        $jackpot1_tradicional="";
                    }
                    try{
                        $jackpot2_tradicional=$loto->jackpot2_tradicional;
                    }catch (Exception $e){
                        $jackpot2_tradicional="";
                    }
                @endphp
                <h3>Jackpot</h3>
                <div class="form-group">
                    <label for="jackpot1_tradicional">Jackpot #1</label>
                    <input type="text" class="form-control" id="jackpot1_tradicional" name="jackpot1_tradicional"
                           value="{{$jackpot1_tradicional}}"/>
                </div>
                <div class="form-group">
                    <label for="jackpot2_tradicional">Jackpot #2</label>
                    <input type="text" class="form-control" id="jackpot2_tradicional" name="jackpot2_tradicional"
                           value="{{$jackpot2_tradicional}}"/>
                </div>

                @php
                    $tradicional_prizes=$loto->tradicional_prizes;
                    $array_labels=[
                        '6 aciertos + 2 Jackpot',
                        '6 aciertos + 1 Jackpot',
                        '6 aciertos',
                        '5 aciertos + 2 Jackpot',
                        '5 aciertos + 1 Jackpot',
                        '5 aciertos',
                        '4 aciertos + 2 Jackpot',
                        '4 aciertos + 1 Jackpot',
                        '4 aciertos',
                        '3 aciertos + 2 Jackpot',
                        '3 aciertos + 1 Jackpot',
                        '3 aciertos'
                    ];
                @endphp

                @for($i=0;$i<12;$i++)
                    @php
                        try{
                            $prize_row=$tradicional_prizes[$i];
                            $label=$prize_row['label'];
                            $winners=$prize_row['winners'];
                            $prize=$prize_row['prize'];
                        }catch (Exception $e){
                            $label=$array_labels[$i];
                            $winners=0;
                            $prize=0;
                        }
                    @endphp
                    <div class="form-group">
                        <input type="hidden" id="tradicional_labels[]" name="tradicional_labels[]" value="{{$label}}"/>
                        <label for="winners[] prizes[]">{{$label}}</label>
                        <input type="text" class="span6" id="tradicional_winners[]" name="tradicional_winners[]"
                               value="{{$winners}}"/>Ganadores
                        <input type="text" class="span6" id="tradicional_prizes[]" name="tradicional_prizes[]"
                               value="{{$prize}}"/>Premio
                    </div>
                @endfor
                <h2>Desquite</h2>
                @php
                    $desquite_numbers=$loto->desquite_numbers;
                @endphp
                @for($i=0;$i<6;$i++)
                    @php
                        try{
                            $desquite_number=$desquite_numbers[$i];
                        }catch (Exception $e){
                            $desquite_number="";
                        }
                    @endphp
                    <div class="form-group">
                        <label for="numbers[]">Número #{{$i+1}}</label>
                        <input type="text" class="form-control" id="desquite_numbers[]" name="desquite_numbers[]"
                               value="{{$desquite_number}}"/>
                    </div>
                @endfor
                @php
                    try{
                        $jackpot1_desquite=$loto->jackpot1_desquite;
                    }catch (Exception $e){
                        $jackpot1_dequite="";
                    }
                    try{
                        $jackpot2_desquite=$loto->jackpot2_desquite;
                    }catch (Exception $e){
                        $jackpot2_desquite="";
                    }
                @endphp
                <h3>Jackpot</h3>
                <div class="form-group">
                    <label for="jackpot1_desquite">Jackpot #1</label>
                    <input type="text" class="form-control" id="jackpot1_desquite" name="jackpot1_desquite"
                           value="{{$jackpot1_desquite}}"/>
                </div>
                <div class="form-group">
                    <label for="jackpot2_desquite">Jackpot #1</label>
                    <input type="text" class="form-control" id="jackpot2_desquite" name="jackpot2_desquite"
                           value="{{$jackpot2_desquite}}"/>
                </div>
                @php
                    $desquite_prizes=$loto->desquite_prizes;
                    $array_labels=[
                        '6 aciertos + 2 Jackpot',
                        '6 aciertos + 1 Jackpot',
                        '6 aciertos'
                    ];
                @endphp
                @for($i=0;$i<3;$i++)
                    @php
                        try{
                            $prize_row=$desquite_prizes[$i];
                            $label=$prize_row['label'];
                            $winners=$prize_row['winners'];
                            $prize=$prize_row['prize'];
                        }catch (Exception $e){
                            $label=$array_labels[$i];
                            $winners=0;
                            $prize=0;
                        }
                    @endphp
                    <div class="form-group">
                        <input type="hidden" id="desquite_labels[]" name="desquite_labels[]" value="{{$label}}"/>
                        <label for="winners[] prizes[]">{{$label}}</label>
                        <input type="text" id="desquite_winners[]" name="desquite_winners[]"
                               value="{{$winners}}"/>Ganadores
                        <input type="text" id="desquite_prizes[]" name="desquite_prizes[]"
                               value="{{$prize}}"/>Premio
                    </div>
                @endfor
                <h2>Sale Sale</h2>
                @php
                    $sale_sale_numbers=$loto->sale_sale_numbers;
                @endphp
                @for($i=0;$i<6;$i++)
                    @php
                        try{
                            $sale_sale_number=$sale_sale_numbers[$i];
                        }catch (Exception $e){
                            $sale_sale_number="";
                        }
                    @endphp
                    <div class="form-group">
                        <label for="sale_sale_numbers[]">Número #{{$i+1}}</label>
                        <input type="text" class="form-control" id="sale_sale_numbers[]" name="sale_sale_numbers[]"
                               value="{{$sale_sale_number}}"/>
                    </div>
                @endfor
                @php
                    try{
                        $jackpot1_sale_sale=$loto->jackpot1_sale_sale;
                    }catch (Exception $e){
                        $jackpot1_sale_sale="";
                    }
                    try{
                        $jackpot2_sale_sale=$loto->jackpot2_sale_sale;
                    }catch (Exception $e){
                        $jackpot2_sale_sale="";
                    }
                    try{
                        $jackpot3_sale_sale=$loto->jackpot3_sale_sale;
                    }catch (Exception $e){
                        $jackpot3_sale_sale="";
                    }
                    try{
                        $jackpot4_sale_sale=$loto->jackpot4_sale_sale;
                    }catch (Exception $e){
                        $jackpot4_sale_sale="";
                    }
                @endphp
                <h3>Jackpot</h3>
                <div class="form-group">
                    <label for="jackpot1_sale_sale">Jackpot #1</label>
                    <input type="text" class="form-control" id="jackpot1_sale_sale" name="jackpot1_sale_sale"
                           value="{{$jackpot1_sale_sale}}"/>
                </div>
                <div class="form-group">
                    <label for="jackpot2_sale_sale">Jackpot #2</label>
                    <input type="text" class="form-control" id="jackpot2_sale_sale" name="jackpot2_sale_sale"
                           value="{{$jackpot2_sale_sale}}"/>
                </div>
                <div class="form-group">
                    <label for="jackpot3_sale_sale">Jackpot #3</label>
                    <input type="text" class="form-control" id="jackpot3_sale_sale" name="jackpot3_sale_sale"
                           value="{{$jackpot3_sale_sale}}"/>
                </div>
                <div class="form-group">
                    <label for="jackpot4_sale_sale">Jackpot #4</label>
                    <input type="text" class="form-control" id="jackpot4_sale_sale" name="jackpot4_sale_sale"
                           value="{{$jackpot4_sale_sale}}"/>
                </div>
                @php
                    $sale_sale_prizes=$loto->sale_sale_prizes;
                    try{
                        $winners=$sale_sale_prizes['winners'];
                    }catch (Exception $e){
                        $winners=0;
                    }
                    try{
                        $prize=$sale_sale_prizes['prize'];
                    }catch (Exception $e){
                        $prize=0;
                    }
                    try{
                        $duplicador=$loto->sale_sale_duplicador;
                    }catch (Exception $e){
                        $duplicador=0;
                    }
                    try{
                        $prize_next_move=$loto->prize_next_move;
                    }catch (Exception $e){
                        $prize_next_move=0;
                    }
                @endphp
                <div class="form-group">
                    <label for="winners[] prizes[]">6 Aciertos</label>
                    <input type="text" class="span6" id="sale_sale_winners" name="sale_sale_winners"
                           value="{{$winners}}"/>Ganadores
                    <input type="text" class="span6" id="sale_sale_prize" name="sale_sale_prize"
                           value="{{$prize}}"/>Premio
                </div>

                <div class="form-group">
                    <label for="sale_sale_duplicador">Duplicador</label>
                    <input type="text" class="form-control" id="sale_sale_duplicador" name="sale_sale_duplicador"
                           value="{{$duplicador}}"/>
                </div>
                <div class="form-group">
                    <label for="prize_next_move">Próximo Premio</label>
                    <input type="text" class="form-control" id="prize_next_move" name="prize_next_move"
                           value="{{$prize_next_move}}"/>
                </div>

                <input type="hidden" name="id_loto" value="{{$loto->_id}}"/>
                <button type="submit" class="btn btn-primary" id="btnSaveLotoContent">Guardar</button>
            </form>
        </div>
    </div>
</div>