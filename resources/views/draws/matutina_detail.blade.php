<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <form method="post" action="{{route('draws.store.matutina.detail')}}">
                @csrf
                @foreach($matutinas as $index=>$matutina)
                    <div class="form-group">
                        <label for="city_text">{{$matutina->city}}</label>
                        <input type="text" class="form-control" id="values[]" name="values[]"
                               value="{{$matutina->value}}">
                        <input type="hidden" name="cities[]" value="{{$matutina->_id}}"/>
                        <a href="{{route('draws.numbers.matutina',['id_matutina'=>$matutina->_id])}}">Modificar Números</a>
                    </div>
                @endforeach
                <input type="hidden" name="id_draw" value="{{$draw->_id}}"/>
                <button type="submit" class="btn btn-primary" id="btnSaveMatutinaContent">Guardar</button>
            </form>
        </div>
    </div>
</div>