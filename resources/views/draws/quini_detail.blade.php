<div class="container jumbotron">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <form method="post" action="{{route('draws.store.quini.detail')}}">
                @csrf
                @php
                    $tradicional_numbers=$quini->tradicional_numbers;
                @endphp
                <h2>Tradicional Primer Sorteo</h2>
                @for($i=0;$i<6;$i++)
                    @php
                        try{
                            $tradicional_number=$tradicional_numbers[$i];
                        }catch (Exception $e){
                            $tradicional_number="";
                        }
                    @endphp
                    <div class="form-group">
                        <label for="numbers[]">Número #{{$i+1}}</label>
                        <input type="text" class="form-control" id="tradicional_numbers[]" name="tradicional_numbers[]"
                               value="{{$tradicional_number}}"/>
                    </div>
                @endfor
                @php
                    $tradicional_prizes=$quini->tradicional_prizes;
                @endphp
                @for($i=4;$i<=6;$i++)
                    @php
                        try{
                            $prize_row=$tradicional_prizes[$i-4];
                            $hits=$prize_row['hits'];
                            $winners=$prize_row['winners'];
                            $prize=$prize_row['prize'];
                        }catch (Exception $e){
                            $hits=$i;
                            $winners=0;
                            $prize=0;
                        }
                    @endphp
                    <div class="form-group">
                        <input type="hidden" id="tradicional_hits[]" name="tradicional_hits[]" value="{{$hits}}"/>
                        <label for="tradicional_winners[] tradicional_prizes[]">{{$hits}} Aciertos</label>
                        <input type="text" class="span6" id="tradicional_winners[]" name="tradicional_winners[]"
                               value="{{$winners}}"/>Ganadores
                        <input type="text" class="span6" id="tradicional_prizes[]" name="tradicional_prizes[]"
                               value="{{$prize}}"/>Premio
                    </div>
                @endfor
                @php
                    $segunda_vuelta_numbers=$quini->segunda_vuelta_numbers;
                @endphp
                <h2>Tradicional Segunda Vuelta</h2>
                @for($i=0;$i<6;$i++)
                    @php
                        try{
                            $segunda_vuelta_number=$segunda_vuelta_numbers[$i];
                        }catch (Exception $e){
                            $segunda_vuelta_number="";
                        }
                    @endphp
                    <div class="form-group">
                        <label for="numbers[]">Número #{{$i+1}}</label>
                        <input type="text" class="form-control" id="segunda_vuelta_numbers[]"
                               name="segunda_vuelta_numbers[]"
                               value="{{$segunda_vuelta_number}}"/>
                    </div>
                @endfor
                @php
                    $segunda_vuelta_prizes=$quini->segunda_vuelta_prizes;
                @endphp
                @for($i=4;$i<=6;$i++)
                    @php
                        try{
                            $prize_row=$segunda_vuelta_prizes[$i-4];
                            $hits=$prize_row['hits'];
                            $winners=$prize_row['winners'];
                            $prize=$prize_row['prize'];
                        }catch (Exception $e){
                            $hits=$i;
                            $winners=0;
                            $prize=0;
                        }
                    @endphp
                    <div class="form-group">
                        <input type="hidden" id="segunda_vuelta_hits[]" name="segunda_vuelta_hits[]" value="{{$hits}}"/>
                        <label for="segunda_vuelta_winners[] segunda_vuelta_prizes[]">{{$hits}} Aciertos</label>
                        <input type="text" class="span6" id="segunda_vuelta_winners[]" name="segunda_vuelta_winners[]"
                               value="{{$winners}}"/>Ganadores
                        <input type="text" class="span6" id="segunda_vuelta_prizes[]" name="segunda_vuelta_prizes[]"
                               value="{{$prize}}"/>Premio
                    </div>
                @endfor

                <h2>Revancha</h2>
                @php
                    $revancha_numbers=$quini->revancha_numbers;
                @endphp
                @for($i=0;$i<6;$i++)
                    @php
                        try{
                            $revancha_number=$revancha_numbers[$i];
                        }catch (Exception $e){
                            $revancha_number="";
                        }
                    @endphp
                    <div class="form-group">
                        <label for="numbers[]">Número #{{$i+1}}</label>
                        <input type="text" class="form-control" id="revancha_numbers[]" name="revancha_numbers[]"
                               value="{{$revancha_number}}"/>
                    </div>
                @endfor
                @php
                    $revancha_prizes=$quini->revancha_prizes;
                @endphp

                @php
                    $hits='6';
                    try{
                        $winners=$revancha_prizes['winners'];
                    }catch (Exception $e){
                        $winners=0;
                    }
                    try{
                        $prize=$revancha_prizes['prize'];
                    }catch (Exception $e){
                        $prize=0;
                    }
                @endphp
                <div class="form-group">
                    <input type="hidden" id="revancha_hits" name="revancha_hits" value="{{$hits}}"/>
                    <label for="revancha_winners revancha_prizes">{{$hits}} Aciertos</label>
                    <input type="text" class="span6" id="revancha_winners" name="revancha_winners"
                           value="{{$winners}}"/>Ganadores
                    <input type="text" class="span6" id="revancha_prizes" name="revancha_prizes"
                           value="{{$prize}}"/>Premio
                </div>

                <h2>Siempre Sale</h2>
                @php
                    $siempre_sale_numbers=$quini->siempre_sale_numbers;
                @endphp
                @for($i=0;$i<6;$i++)
                    @php
                        try{
                            $siempre_sale_number=$siempre_sale_numbers[$i];
                        }catch (Exception $e){
                            $siempre_sale_number="";
                        }
                    @endphp
                    <div class="form-group">
                        <label for="numbers[]">Número #{{$i+1}}</label>
                        <input type="text" class="form-control" id="siempre_sale_numbers[]"
                               name="siempre_sale_numbers[]" value="{{$siempre_sale_number}}"/>
                    </div>
                @endfor
                @php
                    $siempre_sale_prizes=$quini->siempre_sale_prizes;
                @endphp
                @php
                    $hits='6';
                    try{
                        $winners=$siempre_sale_prizes['winners'];
                    }catch (Exception $e){
                        $winners=0;
                    }
                    try{
                        $prize=$siempre_sale_prizes['prize'];
                    }catch (Exception $e){
                        $prize=0;
                    }
                @endphp
                <div class="form-group">
                    <input type="hidden" id="siempre_sale_hits" name="siempre_sale_hits" value="{{$hits}}"/>
                    <label for="siempre_sale_winners siempre_sale_prizes">{{$hits}} Aciertos</label>
                    <input type="text" class="span6" id="siempre_sale_winners" name="siempre_sale_winners"
                           value="{{$winners}}"/>Ganadores
                    <input type="text" class="span6" id="siempre_sale_prizes" name="siempre_sale_prizes"
                           value="{{$prize}}"/>Premio
                </div>

                <h2>Premio Extra</h2>
                @php
                    try{
                        $premio_extra=$quini->premio_extra;
                    }catch(Exception $exception){
                        $premio_extra="";
                    }
                @endphp
                <div class="form-group">
                    <input type="text" class="form-control" id="premio_extra"
                           name="premio_extra" value="{{$premio_extra}}"/>
                </div>
                @php
                    $premio_extra_prizes=$quini->premio_extra_prizes;
                @endphp
                @php
                    $hits='6';
                    try{
                        $winners=$premio_extra_prizes['winners'];
                    }catch (Exception $e){
                        $winners=0;
                    }
                    try{
                        $prize=$premio_extra_prizes['prize'];
                    }catch (Exception $e){
                        $prize=0;
                    }
                @endphp
                <div class="form-group">
                    <input type="hidden" id="premio_extra_hits" name="premio_extra_hits" value="{{$hits}}"/>
                    <label for="premio_extra_winners premio_extra_prizes">{{$hits}} Aciertos</label>
                    <input type="text" class="span6" id="premio_extra_winners" name="premio_extra_winners"
                           value="{{$winners}}"/>Ganadores
                    <input type="text" class="span6" id="premio_extra_prizes" name="premio_extra_prizes"
                           value="{{$prize}}"/>Premio
                </div>

                @php
                    try{
                        $prize_next_move=$quini->prize_next_move;
                    }catch (Exception $exception){
                        $prize_next_move="";
                    }
                @endphp
                <div class="form-group">
                    <label for="next_prize">Próximo Premio</label>
                    <input type="text" class="form-control" id="prize_next_move" name="prize_next_move"
                           value="{{$prize_next_move}}"/>
                </div>
                <input type="hidden" name="id_quini" value="{{$quini->_id}}"/>
                <button type="submit" class="btn btn-primary" id="btnSaveQuiniContent">Guardar</button>
            </form>
        </div>
    </div>
</div>