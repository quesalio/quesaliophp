<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <form method="post" action="{{route('draws.store.nocturna.detail')}}">
                @csrf
                @foreach($nocturnas as $index=>$nocturna)
                    <div class="form-group">
                        <label for="city_text">{{$nocturna->city}}</label>
                        <input type="text" class="form-control" id="values[]" name="values[]"
                               value="{{$nocturna->value}}">
                        <input type="hidden" name="cities[]" value="{{$nocturna->_id}}"/>
                        <a href="{{route('draws.numbers.nocturna',['id_nocturna'=>$nocturna->_id])}}">Modificar
                            Números</a>
                    </div>
                @endforeach
                <input type="hidden" name="id_draw" value="{{$draw->_id}}"/>
                <button type="submit" class="btn btn-primary" id="btnSaveNocturnaContent">Guardar</button>
            </form>
        </div>
    </div>
</div>