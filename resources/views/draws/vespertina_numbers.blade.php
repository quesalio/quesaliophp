@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
                <h1 class="modal-header">
                    Modificar Números Vespertina - {{$vespertina->city}}
                    - {{$vespertina->date->formatLocalized('%d/%m/%Y') }}
                </h1>
                <form method="post" action="{{route('draws.store.numbers.vespertina')}}">
                    @csrf
                    @if(count($numbers)==0)
                        @for($i=1;$i<=20;$i++)
                            <div class="form-group">
                                <label for="number_text">Número #{{$i}}</label>
                                <input type="text" class="form-control" id="values[]" name="values[]"/>
                                <input type="hidden" name="numbers[]" value="{{$i}}"/>
                            </div>
                        @endfor
                    @else
                        @foreach($numbers as $index=>$number)
                            <div class="form-group">
                                <label for="number_text">Número #{{$number->number}}</label>
                                <input type="text" class="form-control" id="values[]" name="values[]"
                                       value="{{$number->value}}">
                                <input type="hidden" name="numbers[]" value="{{$number->number}}"/>
                            </div>
                        @endforeach
                    @endif
                    <input type="hidden" name="id_vespertina" value="{{$vespertina->_id}}"/>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
@endsection