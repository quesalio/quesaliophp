<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <form method="post" action="{{route('draws.store.vespertina.detail')}}">
                @csrf
                @foreach($vespertinas as $index=>$vespertina)
                    <div class="form-group">
                        <label for="city_text">{{$vespertina->city}}</label>
                        <input type="text" class="form-control" id="values[]" name="values[]"
                               value="{{$vespertina->value}}">
                        <input type="hidden" name="cities[]" value="{{$vespertina->_id}}"/>
                        <a href="{{route('draws.numbers.vespertina',['id_vespertina'=>$vespertina->_id])}}">Modificar Números</a>
                    </div>
                @endforeach
                <input type="hidden" name="id_draw" value="{{$draw->_id}}"/>
                <button type="submit" class="btn btn-primary" id="btnSaveVespertinaContent">Guardar</button>
            </form>
        </div>
    </div>
</div>