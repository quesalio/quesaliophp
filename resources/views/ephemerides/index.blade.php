@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            @if(count($ephemerides)>0)
                <div class="col-lg-12">
                    {{ Form::open(['route' => ['ephemerides.destroy.all'], 'method' => 'delete','class'=>'float-right button-delete-all']) }}
                    <button type="submit" class="btn btn-primary">Eliminar Todos</button>
                    {{ Form::close() }}
                    <div class="block margin-bottom-sm">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Número</th>
                                    <th>Descripción</th>
                                    <th>Imagen</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ephemerides as $index=>$ephemeride)
                                    <tr>
                                        <th scope="row">{{++$index}}</th>
                                        <td>{{$ephemeride->number}}</td>
                                        <td>{{$ephemeride->description}}</td>
                                        <td>{{$ephemeride->image}}</td>
                                        <td>
                                            {{ Form::open(['route' => ['ephemerides.destroy',$ephemeride->_id], 'method' => 'delete','class'=>'form-row']) }}
                                            <a href="{{route('ephemerides.edit',['ephemeride_id'=>$ephemeride->_id])}}">Editar</a>&nbsp;|&nbsp;
                                            <button type="submit" class="btn btn-link btn-delete">Eliminar</button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-lg-12">
                    <div class="alert alert-info" role="alert">
                        No hay registros para mostrar
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection