@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
                <form method="post" action="{{route('ephemerides.update',['ephemerides_id'=>$ephemerides->_id])}}">
                    @csrf
                    <div class="form-group">
                        <label for="number">Número</label>
                        <input type="text" class="form-control" id="number" name="number" placeholder="Número"
                               value="{{$ephemerides->number}}">
                    </div>
                    <div class="form-group">
                        <label for="image">Imagen</label>
                        <input type="text" class="form-control" id="image" name="image" placeholder="Imagen"
                               value="{{$ephemerides->image}}">
                    </div>
                    <div class="form-group">
                        <label for="description">Descripción</label>
                        <textarea class="form-control" id="description" name="description" rows="3"
                                  placeholder="Descripción del efemérides">{{$ephemerides->description}}
                        </textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input name="_method" type="hidden" value="PATCH">
                </form>
            </div>
        </div>
    </div>
@endsection