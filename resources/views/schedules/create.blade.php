@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
                <form method="post" action="{{route('schedule.store')}}">
                    @csrf
                    <div class="form-group">
                        <label for="number">Juego</label>
                        <select name="draw" class="form-control" id="draw">
                            @foreach($draws as $draw)
                                <option value="{{$draw['key']}}">{{$draw['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="day">Día</label>
                        <select name="day" class="form-control" id="day">
                            @foreach($days as $day)
                                <option value="{{$day['number']}}">{{$day['day']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="hour">Hora</label>
                        <input type="time" class="form-control" id="hour" name="hour" placeholder="Hora" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
@endsection