@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            @if(count($schedules)>0)
                <div class="col-lg-12">
                    {{ Form::open(['route' => ['schedule.destroy.all'], 'method' => 'delete','class'=>'float-right button-delete-all']) }}
                    <button type="submit" class="btn btn-primary">Eliminar Todos</button>
                    {{ Form::close() }}
                    <div class="block margin-bottom-sm">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Juego</th>
                                    <th>Día</th>
                                    <th>Hora</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($schedules as $index=>$schedule)
                                    <tr>
                                        <th scope="row">{{++$index}}</th>
                                        <td>
                                            {{$schedule->draw}}
                                        </td>
                                        <td>
                                            @php
                                                switch($schedule->day){
                                                    case '8': echo 'Domingo';break;
                                                    case '2': echo 'Lunes';break;
                                                    case '3': echo 'Martes';break;
                                                    case '4': echo 'Miércoles';break;
                                                    case '5': echo 'Jueves';break;
                                                    case '6': echo 'Viernes';break;
                                                    case '7': echo 'Sábado';break;
                                                }
                                            @endphp
                                        </td>
                                        <td>{{$schedule->hour}}</td>
                                        <td>
                                            {{ Form::open(['route' => ['schedule.destroy',$schedule->_id], 'method' => 'delete','class'=>'form-row']) }}
                                            <button type="submit" class="btn btn-link btn-delete">Eliminar</button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-lg-12">
                    <div class="alert alert-info" role="alert">
                        No hay registros para mostrar
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection