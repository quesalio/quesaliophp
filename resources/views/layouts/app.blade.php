<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Que Salió - Panel de Control') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                Inicio
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                    @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Modificar Sorteos
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('draws.primera')}}">Primera</a>
                                <a class="dropdown-item" href="{{route('draws.matutina')}}">Matutina</a>
                                <a class="dropdown-item" href="{{route('draws.vespertina')}}">Vespertina</a>
                                <a class="dropdown-item" href="{{route('draws.nocturna')}}">Nocturna</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('draws.poceada')}}">Poceada</a>
                                <a class="dropdown-item" href="{{route('draws.poceada.plus')}}">Poceada Plus</a>
                                <a class="dropdown-item" href="{{route('draws.brinco')}}">Brinco</a>
                                <a class="dropdown-item" href="{{route('draws.quini')}}">Quini</a>
                                <a class="dropdown-item" href="{{route('draws.loto')}}">Loto</a>
                            </div>
                        </li>


                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Efemérides
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('ephemerides.create')}}">Agregar</a>
                                <a class="dropdown-item" href="{{route('ephemerides.index')}}">Reportar</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Datos Semanales
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('weekly.create')}}">Modificar</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Configuración
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('configuration.create')}}">Modificar</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item"
                                   href="{{route('configuration.start.draw',['draw'=>'primera'])}}">Iniciar Primera
                                </a>
                                <a class="dropdown-item"
                                   href="{{route('configuration.start.draw',['draw'=>'matutina'])}}">Iniciar Matutina
                                </a>
                                <a class="dropdown-item"
                                   href="{{route('configuration.start.draw',['draw'=>'vespertina'])}}">Iniciar
                                    Vespertina
                                </a>
                                <a class="dropdown-item"
                                   href="{{route('configuration.start.draw',['draw'=>'nocturna'])}}">Iniciar Nocturna
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('configuration.stop.draw')}}">Detener Sorteo</a>
                            </div>

                        </li>


                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Obtener Sorteos
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('manual.primera')}}">Primera</a>
                                <a class="dropdown-item" href="{{route('manual.matutina')}}">Matutina</a>
                                <a class="dropdown-item" href="{{route('manual.vespertina')}}">Vespertina</a>
                                <a class="dropdown-item" href="{{route('manual.nocturna')}}">Nocturna</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('manual.dollar')}}">Dólar</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('manual.create.schema')}}">Estructura Sorteo</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('manual.poceada')}}">Poceada</a>
                                <a class="dropdown-item" href="{{route('manual.poceada_plus')}}">Poceada Plus</a>
                                <a class="dropdown-item" href="{{route('manual.quini')}}">Quini</a>
                                <a class="dropdown-item" href="{{route('manual.loto')}}">Loto</a>
                                <a class="dropdown-item" href="{{route('manual.brinco')}}">Brinco</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Recomendaciones
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('recomendation.index')}}">Reportar</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Cal
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{route('schedule.create')}}">Agregar</a>
                                <a class="dropdown-item" href="{{route('schedule.index')}}">Reportar</a>
                            </div>
                        </li>

                    @endauth

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>
</html>
