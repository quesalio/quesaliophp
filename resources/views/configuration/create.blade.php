@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
                <form method="post" action="{{route('configuration.store')}}">
                    @csrf
                    <div class="form-group">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="showEphemerides" name="showEphemerides"
                                   @isset($configuration)
                                   @if($configuration->showEphemerides=='1')
                                   checked
                                    @endif
                                    @endisset
                            >
                            <label class="form-check-label" for="showEphemerides">Mostrar Efemérides</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="showNumbers" name="showNumbers"
                                   @isset($configuration)
                                   @if($configuration->showNumbers=='1')
                                   checked
                                    @endif
                                    @endisset
                            >
                            <label class="form-check-label" for="showNumbers">Mostrar Datos Semanales</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="showRecomendations"
                                   name="showRecomendations"
                                   @isset($configuration)
                                   @if($configuration->showRecomendations=='1')
                                   checked
                                    @endif
                                    @endisset
                            >
                            <label class="form-check-label" for="showNumbers">Mostrar Recomendaciones</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date">Fecha Actual</label>
                        <input type="date" class="form-control" id="date" name="date"
                               placeholder="Fecha actual para sorteos"
                                @php
                                    try{
                                    $date=$configuration->date;
                                    $value=$date->year.'-'.str_pad($date->month,2,'0',STR_PAD_LEFT).'-'.str_pad($date->day,2,'0',STR_PAD_LEFT);
                                    if($value!=null){
                                        echo 'value="'.$value.'"';
                                    }}catch (Exception $exception){}
                                @endphp
                        >
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
@endsection