@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
                <form method="post" action="{{route('weekly.store')}}">
                    @csrf
                    @for($i = 1; $i <=6; $i++)
                        <div class="form-group">
                            <label for="number{{$i}}">Número #{{$i}}</label>
                            <input type="text" class="form-control" id="number{{$i}}" name="number{{$i}}"
                                    @php
                                        try{
                                        $piece="number".$i;
                                        $value=$weekly->$piece;
                                        if($value!=null){
                                            echo 'value="'.$value.'"';
                                        }}catch (Exception $exception){}
                                    @endphp
                            />
                        </div>
                    @endfor
                    <div class="form-group">
                        <label for="date">Fecha</label>
                        <input type="date" class="form-control" id="date" name="date" placeholder="Válido hasta"
                                @php
                                    try{
                                    $date=$weekly->date;
                                    $value=$date->year.'-'.str_pad($date->month,2,'0',STR_PAD_LEFT).'-'.str_pad($date->day,2,'0',STR_PAD_LEFT);
                                    if($value!=null){
                                        echo 'value="'.$value.'"';
                                    }}catch (Exception $exception){}
                                @endphp
                        >
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
@endsection