@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                @if(session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <li>{{$error}}</li>
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            @if(count($recomendations)>0)
                <div class="col-lg-12">
                    {{ Form::open(['route' => ['recomendation.destroy.all'], 'method' => 'delete','class'=>'float-right button-delete-all']) }}
                    <button type="submit" class="btn btn-primary">Eliminar Todos</button>
                    {{ Form::close() }}
                    <div class="block margin-bottom-sm">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Fecha</th>
                                    <th>Nombre</th>
                                    <th>Recomendación</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recomendations as $index=>$recomendation)
                                    <tr>
                                        <th scope="row">{{++$index}}</th>
                                        <td>
                                            {{$recomendation->date->day}}/{{$recomendation->date->month}}
                                            /{{$recomendation->date->year}}
                                        </td>
                                        <td>{{$recomendation->name}}</td>
                                        <td>{{$recomendation->recomendation}}</td>
                                        <td>
                                            {{ Form::open(['route' => ['recomendation.destroy',$recomendation->_id], 'method' => 'delete','class'=>'form-row']) }}
                                            <button type="submit" class="btn btn-link btn-delete">Eliminar</button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-lg-12">
                    <div class="alert alert-info" role="alert">
                        No hay registros para mostrar
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection